# -*- coding: utf-8 -*-
###############################################################################
#       galilee_jeito.py
#       
#       Copyright © 2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bibliothèque d'appel à l'API de Jeito"""

import sys
import csv
import requests
import datetime
from prettytable import PrettyTable

# import de la configuration
sys.path.insert(0, '/etc/galilee_ldap')
import conf

GET=0

# statuts d'adhésion
ADH_STATUS = {
    1: 'OK',
    2: 'PARTI·E',
    3: 'SUSPENDU·E',
    4: 'EXCLU·E',
}
ALERTE_ADH_STATUS = [ADH_STATUS[3], ADH_STATUS[4]]

def _status_id(status):
    """Return the status ID from status string"""
    for stid, ststr in ADH_STATUS.items():
        if ststr == status:
            return stid
    return None

def saison_actuelle(galilee=False):
    """Renvoie l'année N de la saison en cours (N-1 - N)
    
        si galilee=True, renvoie la saison au format Galilée :
            "N-1-N"
    """
    t = datetime.date.today()
    n = t.year + 1 if t.month > 8 else t.year
    if galilee:
        return "{}-{}".format(n-1, n)
    else:
        return n

# class d'accès à l'API jeito pour import du portail
class JeitoAPI:
    """Accès à l'API Jeito pour l'accès aux données du portail"""

    V3URL = "https://jeito.eedf.fr/api/v3/"
    
    def __init__(self):
        """Création d'une nouvelle instance de l'API, récupération de la
        configuration.
        """
        self.jtoken = conf.JEITO_TOKEN
        self._function_types = None

    def _api(self, verb, call, params=None):
        """Fait un GET sur l'API jeito"""
        if verb == GET:
            response = requests.get(self.V3URL + call, params,
                headers={
                    'Authorization': 'token ' + self.jtoken,
                    'User-Agent': 'Galilée LDAP',
                }, stream=True)        
        return response.json()            

    def _api_generator(self, *args, **kwargs):
        """Appele l'API avec *args, **kwargs, boucle sur la réponse en utilisant
        les paramètres 'limit' et 'offset'
        
        Produit un générateur, possible d'itirér sur cette fonction
        """
        offset = 0
        limit = 1000
        finished = False
        
        if 'params' in kwargs:
            params = kwargs['params'].copy()
        else:
            params = {}
        
        while not finished:
            # build the request
            kwargs['params'] = params.copy()
            kwargs['params']['limit'] = str(limit)
            kwargs['params']['offset'] = str(offset)
            
            data = self._api(*args, **kwargs)
            count = data['count']
            # return each individual result
            for res in data['results']:
                yield res
            # grow the offset
            offset += limit
            if offset > count:
                finished = True

    def _api_dict(self, *args, **kwargs):
        """Appelle l'API avec *args, **kwargs, récupère tous les items
        en bouclant avec `limit` et `offset`, et retourne un dictionnaire
        avec les `id` en clefs.
        """
        d = {}
        for elmt in self._api_generator(*args, **kwargs):
            d[elmt['id']] = elmt
        return d

    def adhesions(self, quiet=False):
        """Renvoi les adhesions connues par Jeito"""
        
        ## fonctions auxiliaires
        
        def ajout_structure(person, structure):
            """Ajoute les identifiant LDAP de structure à la personne :
                garde la première OU donnée, et ajoute les O en liste
            """
            if not 'ou' in person:
                person['ou'] = structure['ou']
            if not 'o' in person:
                person['o'] = [structure['o']]
            elif not structure['o'] in person['o']:
                person['o'].append(structure['o'])
                
        def display_name(person):
            """Retourne un displayName à partir d'une entrée de person"""
            return '{} {}'.format(
                person['first_name'].title(),
                person['last_name'].title(),
            )

        def ajout_resp_legaux(person_dict, person_id):
            """Complète les attributs respLegauxMail et respLegauxJeitoID"""
            def _ajout_legal(lg_id, resp_ids, resp_mails):
                resp_ids.append(lg_id)
                mails = person_dict[lg_id]['email'] 
                if isinstance(mails, list): 
                    resp_mails = resp_mails + mails 
                elif mails: 
                    resp_mails.append(mails)
            
            resp_ids = []
            resp_mails = []
            
            person = person_dict[person_id]
            if 'legal_guardian1_id' in person:
                _ajout_legal(person['legal_guardian1_id'], resp_ids, resp_mails)
            if 'legal_guardian2_id' in person:
                _ajout_legal(person['legal_guardian2_id'], resp_ids, resp_mails)
            
            if resp_ids:
                person['respLegauxJeitoID'] = resp_ids
                person['respLegauxMail'] = resp_mails


        def ajout_fonction(person_dict, fonction):
            """Ajoute une fonction à une personne.
                Vérifie qu'il n'y ait pas de doublon sans sensibilité à la casse
            """
            if not 'title' in person_dict:
                person_dict['title'] = [fonction]
            else:
                # vérifie les doublons
                for fonc_name in person_dict['title']:
                    if fonc_name.lower() == fonction.lower():
                        return # déjà là
                person_dict['title'].append(fonction)

        ## récupération des adhésions

        p_seas = {'season': saison_actuelle()}
        
        # d'abord, on récupère les éléments de hiérarchie (structures, équipes)
        # récupérer les structures
        if not quiet: print('Récupérer les structures')
        struct_dict = self.structures()

        # récupérer les équipes
        if not quiet: print('Récupérer les équipes')
        team_dict = self._api_dict(GET, 'teams/')
                
        # récupérer les person_id
        if not quiet: print('Récupérer les fiches personnes…')
        person_dict = self._api_dict(GET, 'persons/')
        
        #### Adhérent·es
        
        # récupérer les adhérents (pour avoir les person_id)
        if not quiet: print('Récupérer les adhérent·es…')
        adherents_dict = self._api_dict(GET, 'adherents/', params=p_seas.copy())
        
        # récupérer toutes les personnes avec des mesures de suspensions/radiation
        if not quiet: print('Récupérer les personnes avec des mesures de suspension/radiation')
        adh_dict_alerte = self._api_dict(GET, 'adherents/', 
            params={'status': '3,4'}, # Suspendu·e,Exclu·e 
        )
        
        # Pour chaque adhésion non-annulées de la saison en cours :
        # donne : structure_id + adherent_id
        if not quiet: print('Ajouter les adhésions aux personnes…')
        for adh in self._api_generator(GET, 'adhesions/', params=p_seas.copy()):
            if not adh['canceled']:
                adherent_id = adh['adherent_id']
                try:
                    person_id = adherents_dict[adherent_id]['person_id']
                    
                    # ajouter le numéro d'adhésion à la personne
                    person_dict[person_id]['cn'] = adherent_id
                except KeyError:
                    # pas trouvé l'adhérent qui correspond à ce numéro d'adhésion
                    # on ne fait rien (adhésion à cheval sur le moment de la requête ?)
                    pass
                else:
                    # ajouter son statut d'adhérent·e et son id d'adherent
                    person_dict[person_id]['adh_status'] = ADH_STATUS[adherents_dict[adherent_id]['status']]
                    person_dict[person_id]['adherent_id'] = adherent_id
                
                    # ajouter les identifiants de structures à cette personne
                    structure_id =  adh['structure_id']
                    ajout_structure(person_dict[person_id], struct_dict[structure_id])
                    
                    # ajouter la saison actuelle
                    person_dict[person_id]['membershipSeason'] = saison_actuelle(galilee=True)
                    
                    # ajouter le displayName, changer la casse des noms
                    person_dict[person_id]['displayName'] = display_name(person_dict[person_id])
                    person_dict[person_id]['first_name'] = person_dict[person_id]['first_name'].title()                                    
                    person_dict[person_id]['last_name'] = person_dict[person_id]['last_name'].title()
                    
                    # si l'adhérent·e a des responsables légaux, ajouter leus id + mail
                    try:
                        ajout_resp_legaux(person_dict, person_id)
                    except KeyError:
                        # pas trouvé les responsables légaux, on ignore
                        # (adhésion à cheval sur le moment de la requête ?)
                        pass
        
        # Pour chaque adhérent·e avec des mesures de suspension/radiation
        alerte_list = []
        for adherent_id in adh_dict_alerte:
            try:
                person_id = adh_dict_alerte[adherent_id]['person_id']
                # ajouter le numéro d'adhésion à la personne
                person_dict[person_id]['cn'] = adherent_id
            except KeyError:
                pass
            else:
                # ajouter son statut d'adhérent·e et son id d'adherent
                person_dict[person_id]['adh_status'] = ADH_STATUS[adh_dict_alerte[adherent_id]['status']] 
                person_dict[person_id]['adherent_id'] = adherent_id 
                 
                # ajouter le displayName, changer la casse des noms 
                person_dict[person_id]['displayName'] = display_name(person_dict[person_id]) 
                person_dict[person_id]['first_name'] = person_dict[person_id]['first_name'].title()                                    
                person_dict[person_id]['last_name'] = person_dict[person_id]['last_name'].title()
                alerte_list.append(person_id)
        
        sa_sc_id_list = []
        
        # Pour chaque salarié·e
        if not quiet: print('Ajouter des numéros salarié·es/SC aux salarié·es/SC non adhérent·es')
            # si non adhérent·e : ajouter un numéro de salarié·e
        for emp in self._api_generator(GET, 'employees/', params=p_seas.copy()):
            person_id = emp['person_id']
            if not 'cn' in person_dict[person_id]:
                person_dict[person_id]['cn'] = 'SAL-{}'.format(emp['id'])
            # on ajoute également le numéro de salarié·e
            person_dict[person_id]['employeeNumber'] = emp['id']
            # utiliser également plutôt les coordonnées pro que perso
            if emp['email']:
                person_dict[person_id]['email'] = emp['email']
            if emp['phone']:
                person_dict[person_id]['mobile_phone'] = emp['phone']
            # ajouter la saison actuelle
            person_dict[person_id]['membershipSeason'] = saison_actuelle(galilee=True)
            # ajouter le displayName, changer la casse des noms
            person_dict[person_id]['displayName'] = display_name(person_dict[person_id])
            person_dict[person_id]['first_name'] = person_dict[person_id]['first_name'].title()                                    
            person_dict[person_id]['last_name'] = person_dict[person_id]['last_name'].title()
            
            sa_sc_id_list.append(person_id)
        
        # pour chaque fonction
        for fonc in self._api_generator(GET, 'functions/', params=p_seas.copy()):
            person_id = fonc['person_id']
            # récupérer l'équipe
            team_id = fonc['team_id']
            # récupérer la structure
            struct_id = team_dict[team_id]['structure_id']
            # vérifier que la fonction est en cours de validité
            today = datetime.date.today()
            try:
                begin = datetime.date.fromisoformat(fonc["begin"])
            except TypeError:
                begin = None
            try:
                end = datetime.date.fromisoformat(fonc["end"])
            except TypeError:
                end = None
            if (not begin or begin <= today) and (not end or today <= end):
                # ajouter la structure si besoin
                try:
                    ajout_structure(person_dict[person_id], struct_dict[struct_id])
                except KeyError:
                    # personne non trouvée… adhésion en cours et pas fini,
                    # probablement
                    pass
                else:
                    # ajouter la fonction (LDAP:title)
                    ajout_fonction(person_dict[person_id], fonc['name'])
                    # ajouter la fonction stable
                    ajout_fonction(
                        person_dict[person_id],
                        self.fonction_stable(fonc, team_dict[team_id])
                    )
            else:
                pass
                #if not quiet:
                #    print('#{} {} {} > Fonction obsolète "{}" ({} à {})'.format(
                #        person_id,
                #        person_dict[person_id]['first_name'],
                #        person_dict[person_id]['last_name'],
                #        fonc['name'],
                #        fonc['begin'],
                #        fonc['end'],
                #    ))
        
        # vérifier que toustes les salarié·es/SC sont rataché·es à une structure
        # pareil pour les personnes qui ne sont plus adhérentes mais ont une
        # mesure de suspension/exclusion
        def_ou = conf.DEF_STRUC
        def_o = conf.STRUCTURES[def_ou][2]
        for s_id in (sa_sc_id_list + alerte_list):
            if not 'ou' in person_dict[s_id]:
                person_dict[s_id]['ou'] = def_ou
            if not 'o' in person_dict[s_id]:
                person_dict[s_id]['o'] = [def_o]
        
        return person_dict

    def structures(self):
        """Renvoie les structures connues par Jeito
        """
        # récupérer les types de structures
        struct_types_dict = self._api_dict(GET, 'structure_types/')
        
        def est_superstructure(s_type):
            """Renvoie True si la structure est d'échelon 1 (asso) ou
            d'échelon 2 (région), False sinon"""
            s_t = struct_types_dict[s_type]
            return (s_t['echelon'] == 1 or s_t['echelon'] == 2)
        
        # récupérer les structures
        s_dict = {}
        for s in self._api_generator(GET, 'structures/'):
            s['superstructure'] = est_superstructure(s['type_id'])
            s_dict[s['id']] = s
        
        # pour chaque structure, ajouter un attribut ou= et un attribut o=
        def _conv_ou(name):
            """Retourne le nom de l'ou, avec une conversion pour les noms
                historiques de Galilée"""
            for struct_id in conf.STRUCTURES:
                if conf.STRUCTURES[struct_id][2] == name:
                    return struct_id
            return name
        
        for s_id, s in s_dict.items():
            if s['superstructure']:
                s['ou'] = _conv_ou(s['name'])
                s['o'] = s['name']
            else:
                parent_s = s_dict[s['parent_id']]
                s['ou'] = _conv_ou(parent_s['name'])
                s['o'] = s['name']
        
        return s_dict
        
    def fonction_stable(self, fonction, equipe):
        """Renvoie un nom de fonction stable basé sur le type de fonction et l'équipe"""
        if not self._function_types:
            # mettre en cache le dictionnaire des types de fonction
            self._function_types = self._api_dict(GET, 'function_types/')
        
        ftype = self._function_types[fonction['type_id']]
        # cas particuliers de nom
        if ftype['id'] == 2:
            fname = 'Membre bénévole'
        else:
            fname = ftype['name']
        
        return '{cat_id}.{type_id} {type_name} [{team_name}]'.format(
            cat_id=ftype['category'],
            type_id=ftype['id'],
            type_name=fname,
            team_name=equipe['name'],
        )

    def fonctions(self):
        """Renvoie deux listes :
            (
                la liste des fonctions connues par Jeito pour l'année en cours,
                la liste des fonctions stables contstruites à partir de la 1er
            )
        """
        team_dict = self._api_dict(GET, 'teams/')
        
        fonc_set = set()
        stable_fonc_set = set()
        params = {'season': saison_actuelle()}
        for fonc in self._api_generator(GET, 'functions/', params=params):
            # ajouter la fonction jeito
            fonc_set.add(fonc['name'])
            # ajoute la fonction stable
            stable_fonc_set.add(self.fonction_stable(fonc, team_dict[fonc['team_id']]))

        return (fonc_set, stable_fonc_set)

# Outil de gestion des alertes des adhérent·es
class GestionAlertes:
    """Classe avec les outils de gestion des alertes des adhérent·es"""
    
    FIELD_NAMES = ['person_id', 'ldap_cn', 'statut_num', 'statut_str', 'date', 'gere_le', 'par_qui', 'contact', 'commentaire']

    def __init__(self):
        """Initialise le gestionnaire d'alertes, ouvre sa base de donnée des
        alertes"""
        self.alertes_connues = {}
        # ouvrir base de donnée des alertes
        try:
            with open(conf.ALERTES_DB, newline='') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    row['person_id'] = int(row['person_id'])
                    row['statut_num'] = int(row['statut_num'])
                    self.alertes_connues[row['person_id']] = row
        except FileNotFoundError:
            # le fichier n'existe pas, pas grave.
            pass

    def save(self):
        """Enregistrer la base de donnée des alertes"""
        # enregistrer la base de donnée des alertes
        with open(conf.ALERTES_DB, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self.FIELD_NAMES)

            writer.writeheader()
            for person_id, person in self.alertes_connues.items():
                writer.writerow(person)
    
    def tester_alertes(self, person):
        """Tester si la personne a un statut d'alerte,
            Ajout de la personne à la base de donnée d'alertes si nécessaire
            Renvoie (ancien_etat, nouvel_etat, est_gere) si la personne a
                un statut à alerte
            Renvoie None sinon
        """
        if not 'id' in person:
            # {'detail': 'No Person matches the given query.'}
            return None
        # si la personne est déjà dans le fichier des suspensions/radiations
        if person['id'] in self.alertes_connues:
            try:
                nouveau_statut = person['adh_status']
            except KeyError:
                nouveau_statut = 'Non-adhérent·e (salarié·e ?)'
            p_connue = self.alertes_connues[person['id']]
            # avec le même statut que maintenant (géré ou pas géré)
            if nouveau_statut == p_connue['statut_str']:
                if p_connue['gere_le']:
                    return (person['adh_status'], person['adh_status'], True) 
                else:
                    return (person['adh_status'], person['adh_status'], False)
            else :
                # avec un autre statut à alerte ou un autre statut sans alerte
                return (p_connue['statut_str'], nouveau_statut, False)
                
        else:
            # si la personne n'est pas déjà dans le fichier des suspension/radiation
            # et a un statut à alerte :
                # dire qu'il y a une alerte
            try:
                if person['adh_status'] in ALERTE_ADH_STATUS:
                    self.alertes_connues[person['id']] = {
                        'person_id': person['id'],
                        #'adherent_id': person['adherent_id'],
                        'ldap_cn': person['cn'],
                        'statut_num': _status_id(person['adh_status']),
                        'statut_str': person['adh_status'],
                        'date': datetime.date.today().isoformat(),
                        'gere_le': None,
                    }
                    return ('OK', person['adh_status'], False) 
            except KeyError:
                # pas de statut d'adhésion (probablement un·e salarié·e)
                pass
            return None

    def commande_gestion_alerte(self, cn=None):
        """Renvoie un message d'aide de la commande pour gérer des alertes"""
        if cn and cn.startswith('cn='):
            cn = cn[3:]
        return """\
Pour gérer une alerte, une personne de l'InterComCom doit :
1. Envoyer un mail comme suit au Comité Directeur (ne divulguant AUCUNE
   info personnelle) :
       
   À : Comité Directeur <comitedirecteur@eedf.fr>
   Sujet : Mesure administrative et outils numériques
   Contenu :
   Bonjour,
   
   À l'InterComCom, nous venons d'avoir une alerte concernant un·e adhérent·e
   qui a été notée comme suspendu·e ou radié·e dans jeito (nous ne précisons
   pas dans ce mail les infos concernant la personne).
   
   Pouvons-nous avoir une personne contact à qui nous pouvons donner
   l'identité de la personne et avec qui nous pourrions discuter des mesures
   à prendre par rapport au compte Galilée de cette personne ?
   
   Merci à vous,
   
2. contacter cette personne référente pour demander s'il y a des mesures à
   prendre concernant Galilée pour la personne concernée, en donnant son
   numéro d'adhérent·e : {numadh}.
   
(Attention : il s'agit d'informations confidentielles, à ne pas divulguer !)

Si besoin de suspendre l'accès au compte de la personne ou de l'empêcher d'en
créer un (sans suppression de données) :
sudo galilee_gestion_ldap alertes --desactiver-cpt {numadh}

Une fois ces mesures prises, il faut dire que c'est fait avec cette commande :
sudo galilee_gestion_ldap alertes --fait {numadh}
        """.format(numadh=(cn if cn else 'XXXXX'))

    def message_alerte(self, person, alerte_statut):
        """Renvoie le message d'alerte correspondant à `alerte_statut`.
        
            `alerte_statut` peut être None,
            ou (ancien_etat, nouvel_etat, est_gere)
        """
        if not alerte_statut:
            return None
        ancien_etat, nouvel_etat, est_gere = alerte_statut
        
        if ( nouvel_etat in ALERTE_ADH_STATUS and
                ( (ancien_etat not in ALERTE_ADH_STATUS) or
                  nouvel_etat == ancien_etat)) :
            if est_gere:
                return None
            else:
                return (
                    "="*30 + "\n"
                    + "Alerte : cn={} est {}".format(person['cn'], nouvel_etat.lower())
                    + "\n\n" + self.commande_gestion_alerte(person['cn'])
                    + "\n" + "="*30 + "\n"
                )
        
        if ancien_etat in ALERTE_ADH_STATUS and nouvel_etat in ALERTE_ADH_STATUS:
            return (
                "="*30 + "\n"
                + "Alerte : cn={} était {}, et est maintenant {}.".format(
                    person['cn'], ancien_etat.lower(), nouvel_etat.lower())
                + "\n\n" + self.commande_gestion_alerte(person['cn'])
                + "\n" + "="*30 + "\n"
            )
        
        if ancien_etat in ALERTE_ADH_STATUS and nouvel_etat not in ALERTE_ADH_STATUS:
            return (
                "="*30 + "\n"
                + "Fin d'alerte : cn={} était {}, et est maintenant {}.".format(
                    person['cn'], ancien_etat.lower(), nouvel_etat.lower())
                + "\n\n" + self.commande_gestion_alerte(person['cn'])
                + "\n" + "="*30 + "\n"
            )
        
        # TODO: ne doit plus pouvoir arriver
        return "Alerte cn={}, cas non géré".format(person['cn'])
        
    def recharger_alerte(self, jeito_api, person_id, ldap_cn):
        """Recharge via jeito les données d'une personne pour savoir quel est
        son état d'alerte.
        """
        person_data = jeito_api._api(GET, 'persons/{}/'.format(person_id))
        
        try:
            adherent_id = int(ldap_cn)
        except ValueError:
            # le cn n'est pas un num adh (id salarié·e ??)
            # pas d'adh data
            pass
        else:
            adh_data = jeito_api._api(GET, 'adherents/{}/'.format(adherent_id))
            if 'id' in adh_data:
                # si adh_data = {'detail': 'Pas trouvé.'}, la personne n'est
                # pas adhérente (salariée ?)
                person_data['adh_status'] = ADH_STATUS[adh_data['status']]
                person_data['adherent_id'] = adherent_id
        
        return self.tester_alertes(person_data)
        
    def synthese_alerte(self, jeito_api):
        """Renvoie un tableau de synthèse des alertes"""
        synthese = PrettyTable()
        synthese.field_names = ["Adhérent·e", "Ancien statut", "Nouveau statut", "Depuis", "Géré ?"]
        for person_id, p_connue in self.alertes_connues.items():
            alerte_statut = self.recharger_alerte(jeito_api, person_id, self.alertes_connues[person_id]['ldap_cn'])
            if alerte_statut:
                ancien_etat, nouvel_etat, est_gere = alerte_statut
                if est_gere:
                    gestion = 'Par {par_qui} le {gere_le}, contact EEDF {contact}, « {commentaire} »'.format(**p_connue)
                else:
                    # pas géré
                    if ancien_etat == nouvel_etat:
                        gestion = "Non"
                    elif ancien_etat in ALERTE_ADH_STATUS and nouvel_etat in ALERTE_ADH_STATUS:
                        gestion = "Non, toujours en alerte mais changement de statut"
                    elif ancien_etat in ALERTE_ADH_STATUS and nouvel_etat not in ALERTE_ADH_STATUS:
                        gestion = "Non, alerte finie (redonner des droits ?)"
                    else:
                        gestion = "Non"
                synthese.add_row([
                    'cn={}'.format(p_connue['ldap_cn']), ancien_etat, nouvel_etat,
                    p_connue['date'],
                    gestion
                ])
        return synthese.get_string()
    
    def alerte_geree(self, jeito_api, cn, par_qui, contact, commentaire):
        """Marquer une alerte comme gérée"""
        # trouver le person_id
        for person_id, p_connu in self.alertes_connues.items():
            if 'cn={}'.format(p_connu['ldap_cn']) == cn:
                alerte_statut = self.recharger_alerte(jeito_api, person_id, p_connu['ldap_cn'])
                if alerte_statut:
                    ancien_etat, nouvel_etat, est_gere = alerte_statut
                    if ancien_etat == nouvel_etat and est_gere:
                        return '{} est déjà marqué comme géré.'.format(cn)
                    elif nouvel_etat in ALERTE_ADH_STATUS:
                        p_connu['statut_num'] = _status_id(nouvel_etat)
                        p_connu['statut_str'] = nouvel_etat
                        p_connu['gere_le'] = datetime.date.today().isoformat()
                        p_connu['par_qui'] = par_qui
                        p_connu['contact'] = contact
                        p_connu['commentaire'] = commentaire
                        self.save()
                        return "L'alerte de {} a été marquée comme gérée dans la liste des personnes avec alerte.".format(cn)
                    elif nouvel_etat not in ALERTE_ADH_STATUS:
                        # supprimer de la DB
                        del(self.alertes_connues[person_id])
                        self.save()
                        return '{} a été supprimé·e de la liste des personnes avec alerte (fin d\'alerte)'.format(cn)
                    else:
                        return '!!! cas non géré, rien n\'a été fait'
                return '{} non trouvé·e dans les personnes concernées par des alertes'.format(cn)
        return '{} non trouvé·e dans les personnes concernées par des alertes'.format(cn)
