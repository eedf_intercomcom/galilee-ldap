# -*- coding: utf-8 -*-
###############################################################################
#       galilee_ldap.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2009-2020, Olivier Radisson
#       Copyright © 2020-2024, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-seafile-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bibliothèque de gestion des groupes Seafile avec le LDAP de Galilée"""

import os
import sys
import json
import subprocess
import datetime
import smtplib
import secrets
import pymysql

# import de la configuration
sys.path.insert(0, '/etc/galilee_ldap')
import galilee_seafile_conf as conf
# import de python-seafile-webapi
sys.path.insert(0, conf.SEAF_WEBAPI_DIR)
import seafile_webapi
# import de galilee-ldap
import galilee_ldap
from seafile_webapi import NotFound, OperationFailed
import seafile_db

ANNEXE_MIGRATION = getattr(conf, 'ANNEXE_MIGRATION', False)
DONT_SEND_MAIL = getattr(conf, 'DONT_SEND_MAIL', False)
# pour des raisons de compatibilité, on considère par défaut qu'on a une version
# de seafile en dessous de 11, sauf si SEAF11PLUS est dans la config, auquel
# cas on considère qu'on a la version 11
SEAFVERSION = int(getattr(conf, 'SEAFVERSION',
    (11 if getattr(conf, 'SEAF11PLUS', False) else 10)
))

def init_seafile_account(numadh, verbose=True, dry_run=False):
    """Pre-populate one or more seafile account, using the right method
        according to the Seafile version and the used SSO.
        
        Return the list of seafile account `mail_id` corresponding to created
        accounts.
    """
    gldap = galilee_ldap.GalileeLDAP()
    # récupérer les e-mail de numadh
    mail_data = gldap.adh_data(numadh, attrs=['mail', 'uid'])
    try:
        mail_list = mail_data['mail']
        uid = mail_data['uid'][0]
    except KeyError:
        return [] #-> pas de mail, on annule

    # connexion à Seafile
    seaf = seafile_webapi.SeafileClient(conf.SEAF_URL, v12auth=(SEAFVERSION>=12))
    seaf.authenticate(
        conf.SEAF_ROOT_MAIL, 
        token=conf.SEAF_ROOT_TOKEN,
        validate=False 
    ) 

    # version pour Seafile v<11
    if SEAFVERSION < 11:
        for mail in mail_list: 
            # s'assurer qu'un compte seafile existe avec cette adresse 
            if not dry_run: 
                try: 
                    seaf.init_seafile_account(mail) 
                except OperationFailed: 
                    print("{}: impossible de créer un compte seafile (cookie absent)".format(mail), file=sys.stderr) 
            if dry_run or verbose: 
                print("{}<{}>: créer compte seafile (v<11)".format(uid, mail))
        return mail_list
    else: # Seafile 11+
        seaf_sso = SeafileSSO()
        if not conf.OICD_REMPLACE_LDAP:
            # methode nouvelle, mais avec un compte par mail LDAP
            mail_id_list = []
            for mail in mail_list:
                if not dry_run:
                    # création de l'utilisateur
                    try:
                        result = seaf.add_user(mail, secrets.token_urlsafe(32), contact_email=mail)
                    except seafile_webapi.BadPath:
                        # le compte Seafile existe déjà, on récupère son identifiant seafile
                        mail_id = seaf_sso.get_seaf_id_ldap(mail)
                        if mail_id: # vérifier qu'il a bien été initialisé par son SSO
                            mail_id_list.append(mail_id)
                    else:
                        #print('nouvel id: {}'.format(result['email']))
                        # switch vers le LDAP
                        seaf_sso.change_auth_ldap(result['email'], mail)
                        mail_id_list.append(result['email'])
                if dry_run or verbose:
                    print("{}<{}>: créer compte seafile 11+ (SSO:ldap)".format(uid, mail))
            return (mail_id_list if not dry_run else mail_list)
        else:
            mail = mail_list[0]
            mail_id = mail
            # methode nouvelle, avec un seul compte OIDC
            if not dry_run:
                # création de l'utilisateur
                try:
                    result = seaf.add_user(mail, secrets.token_urlsafe(32), login_id=mail, contact_email=mail)
                except seafile_webapi.BadPath:
                    # le compte Seafile existe déjà, on récupère son identifiant seafile
                    mail_id = seaf_sso.get_seaf_id_oidc(uid)
                    if not mail_id: # vérifier qu'il a bien été initialisé par son SSO
                        return []
                else:
                    mail_id = result['email']
                    #print('nouvel id: {}'.format(mail_id))
                    # switch vers OIDC
                    seaf_sso.change_auth_oidc(mail_id, uid)
            if dry_run or verbose:
                print("{}<{}>: créer compte seafile 11+ (SSO:OIDC)".format(uid, mail))
            return [mail_id] 

class SeafileGroupes:
    """Gestion des groupes Seafile de Galilée, en lien avec le LDAP.
    """
    
    def __init__(self):
        """Initialise une connexion en root au LDAP et à Seafile"""
        self.gldap = galilee_ldap.GalileeLDAP()
        self.seaf = seafile_webapi.SeafileClient(conf.SEAF_URL, v12auth=(SEAFVERSION>=12))
        self.seaf.authenticate(
            conf.SEAF_ROOT_MAIL,
            token=conf.SEAF_ROOT_TOKEN,
            validate=False
        )
        self._group_data = {}
    
    def _load_group_data(self):
        """Load group data from the disk, merge it with group config"""
        # load BDD
        try:
            with open(conf.GRP_BDD, 'r') as fileo:
                self._group_data = json.load(fileo)
        except FileNotFoundError:
            self._group_data = {}
        # overwrite with conf data
        self._group_data.update(conf.GROUPES_CORRESPONDANCE)
        
    def _save_group_data(self):
        """Save group data to the disk"""
        with open(conf.GRP_BDD, 'w') as fileo:
            fileo.write(json.dumps(self._group_data))

    def ajouter_groupes_par_defaut(self, numadh, dry_run=False, verbose=False):
        """Ajouter `numadh` aux groupes par défault
        
            Si dry_run=True, affiche les opération dans la console au lieu de
            les faire.
        """
        if conf.FORCE_DRY_RUN:
            dry_run = True
        
        def valid_group_name(name):
            """Return a valid group name"""
            v_name = ''
            for c in name:
                if c.isalnum() or c in ' -_':
                    v_name += c
            return v_name
        
        # recharger les données des groupes
        if not (dry_run and self._group_data):
            self._load_group_data()
        
        # récupérer les identifiants de numadh
        try:
            mail_data = self.gldap.adh_data(numadh, attrs=['mail', 'uid'])
        except galilee_ldap.NumAdhInconnu:
            return # numéro adh inconnu -> on annule
        try:
            mail_list = mail_data['mail']
            uid = mail_data['uid'][0]
        except KeyError:
            return # pas de mail -> on annule
        
        # déterminer la liste des structures auxquelles appartient cette personne
        struct_list = []
        ou, o = None, None
        for part in self.gldap.dn(numadh).split(','):
            if part.startswith('ou='):
                # si l'OU est futuraaee -> ajouter à la structure de base
                if part.endswith('-futuraaee'):
                    part = part[:-10]
                ou = part
                try:
                    ou_name = valid_group_name(galilee_ldap.conf.STRUCTURES[part[3:]][1])
                except KeyError:
                    # cette ou= n'est pas dans la liste des structures de conf.
                    ou_name = valid_group_name(part[3:].title())
                # si l'OU est guest, on n'ajoute pas.
                if not part.endswith(galilee_ldap.conf.GUEST_OU): 
                    struct_list.append((ou, ou_name))
                break
        o_data = self.gldap.adh_data(numadh, attrs=['o'])
        if o_data:
            for o in o_data['o']:
                o = o.upper() # upper -> normalize data
                o_name = valid_group_name(o.title())
                o = 'o={}'.format(o)
                # vérifier que o n'est pas une structure identifée dans la conf LDAP
                for struct in galilee_ldap.conf.STRUCTURES:
                    if galilee_ldap.conf.STRUCTURES[struct][2] == o[2:]:
                        o = 'ou={}'.format(struct)
                        o_name = valid_group_name(galilee_ldap.conf.STRUCTURES[struct][1])
                        break
            
                # vérifier que o n'est pas la même structure que ou pour l'ajouter
                if o != ou:
                    struct_list.append((o, o_name))
        
        # s'assurer que le/les comptes sefaile existent
        seaf_id_list = init_seafile_account(numadh, verbose, dry_run)
        
        for seaf_id in seaf_id_list:
            # pour chaque structure
            for struct, struct_name in struct_list:
                if struct in self._group_data:
                    # la structure existe !
                    for group in self._group_data[struct]:
                        group_id, group_name = group
                        # ajouter au groupe
                        if not dry_run:
                            try:
                                self.seaf.add_user_in_group(group_id, seaf_id, admin=True)
                            except NotFound: # le groupe a été supprimé dans Seafile ?
                                # on la supprime de _group_data, pour que le groupe soit re-créé
                                del self._group_data[struct]
                                # on sort de ce for
                                break
                        if dry_run or verbose:
                            print("{}<{}>: ajout au groupe {}({})".format(uid, seaf_id, group_id, group_name))
                        # donner le rôle admin
                        if not dry_run:
                            self.seaf.set_group_admin_status(group_id, seaf_id, admin=True)
                        if dry_run or verbose:
                            print("{}<{}>: donner le statut admin dans le groupe {}".format(uid, seaf_id, group_id))
                if not struct in self._group_data:
                    # le groupe n'existe pas dans les structure connues.
                    new_group = True
                    group_type, group_name = struct.split('=')
                    # appliquer un template de groupe
                    group_name = struct_name
                    # creer le groupe
                    if not dry_run:
                        try:
                            res = self.seaf.create_group(group_name)
                        except:
                            # potentiellement le groupe existe dans seafile,
                            # mais pas dans _group_data
                            res = self.seaf.search_group(group_name)
                            if len(res) == 1:
                                # si un seul groupe de trouvé, c'est bon !
                                group_id = res[0]['id']
                                new_group = False
                            else:
                                # sinon, on ne sait pas quoi faire
                                raise
                        else:
                            group_id = res['id']
                        self._group_data[struct] = [(group_id, group_name)]
                    else:
                        group_id = 4242
                        self._group_data[struct] = [(group_id, group_name)]
                    if dry_run or verbose:
                        if new_group:
                            print("{}<{}>: créer le groupe {} = [({}, {})]".format(uid, seaf_id, struct, group_id, group_name))
                        else:
                            print("{}<{}>: ajout au groupe {} qui existait déjà, récupéré depuis Seafile ({}, {})".format(uid, seaf_id, struct, group_id, group_name)) 
                    # transférer la propriété à cette personne
                    if new_group:
                        if not dry_run:
                            self.seaf.transfer_group(group_id, seaf_id, admin=True)
                        if dry_run or verbose:
                            print("{}<{}>: transférer la propriété du groupe {}".format(uid, seaf_id, group_id))
                    
        # enregistrer les données des groupes
        if not dry_run:
            self._save_group_data()

    def ajouter_tous_groupes_defaut(self, structure=None, dry_run=False, verbose=False):
        """Ajouter toustes les utilisateurices dans leurs groupes par défault.
        
            Si structure est défini, limite l'ajout aux utilisateurices de
            cette structure.
        """
        # récupérer la liste des adhérent·es concerné·es
        if structure:
            numadh_list = self.gldap.recherche_numadh(
                "(&(uid=*)(ou:dn:={}))".format(structure)
            )
        else:
            numadh_list = self.gldap.recherche_numadh('uid=*')

        # leur créer des groupes
        for numadh in numadh_list:
            try:
                self.ajouter_groupes_par_defaut(numadh, dry_run, verbose)
            except ValueError:
                # problème pour les guests qui ont un numéro d'adhérent non int
                pass


class SeafileMigration:
    """Migration de comptes Seafile en cas de changement de mail"""
    
    def __init__(self):
        """Initialise une connexion en root à Seafile et le gestionnaire de SSO"""
        self.seaf = seafile_webapi.SeafileClient(conf.SEAF_URL, v12auth=(SEAFVERSION>=12))
        self.seaf.authenticate(
            conf.SEAF_ROOT_MAIL,
            token=conf.SEAF_ROOT_TOKEN,
            validate=False
        )
        self.seaf_sso = SeafileSSO()
        self.gldap = galilee_ldap.GalileeLDAP()

    def migration_mail(self, ancien_mail, nouveau_mail, galilee_uid, dry_run=False):
        """Change le mail identifiant un compte Seafile
        
        Si un script ANNEXE_MIGRATION est défini dans la configuration, le lance 
        """
        if conf.FORCE_DRY_RUN:
            dry_run = True 
         
        if ancien_mail.lower() == nouveau_mail.lower(): 
            # vérification que les deux identifiants sont bien différents… 
            return        

        if SEAFVERSION >= 11 and conf.OICD_REMPLACE_LDAP:
            # en mode OIDC, plus besoin de faire de migration !!
            if dry_run:
                print("Mode OIDC, pas besoin de migration de compte Seafile")
            return

        # version pour Seafile v<11
        if SEAFVERSION < 11:
            self._migration_mail_seaf10(ancien_mail, nouveau_mail, galilee_uid, dry_run)
        else:
            # version pour Seafile 11+ avec LDAP
            self._migration_mail_seaf11(ancien_mail, nouveau_mail, galilee_uid, dry_run)

        # envoyer un mail d'info sur le changement d'identifiant
        subject = "[EEDF Galilée] Changement d'identifiant de connexion Seafile"
        
        content = """Bonjour,
(ceci est un mail automatique)

Ton adresse e-mail renseignée dans Jeito (fichier adhérent des EEDF) a changé.
Lors de la synchronisation du serveur Galilée avec le fichier adhérent, ton
compte Seafile a été mis à jour, et il faut donc que tu utilise cette nouvelle
adresse e-mail pour te connecter à Seafile. Il se peut qu'il te faille
également re-configurer le logiciel Seafile sur ton ordi et l'appli smartphone
si tu les utilises.

Rappel de tes identifiants de connexion Galilée :
    - Seafile et les formulaire Seafile : {}
    - Tous les autres services de Galilée : {}

Dans tous les cas, c'est le même mot de passe, celui que tu as choisi en créant
ton compte Galilée.

Si tu as des questions, n'hésite pas à les poser aux bénévoles qui gèrent
Galilée, l'InterComCom : intercomcom@md2t.eu

Éclaireusement,
--
Le serveur Galilée des EEDF
"""
        
        content = content.format(nouveau_mail, galilee_uid)
        self._sendmail(conf.FROM_MAIL, nouveau_mail, subject, content, dry_run)

    def _migration_mail_seaf10(self, ancien_mail, nouveau_mail, galilee_uid, dry_run=False):
        """Change le mail identifiant un compte Seafile
        
        Si un script ANNEXE_MIGRATION est défini dans la configuration, le lance
        
        (uniquement pour Seafile v<11)
        """
        if conf.FORCE_DRY_RUN:
            dry_run = True
        
        if ancien_mail.lower() == nouveau_mail.lower():
            # vérification que les deux identifiants sont bien différents…
            return
        
        if not dry_run:
            try:
                self.seaf.init_seafile_account(nouveau_mail)
            except OperationFailed:
                print("{}: pour migration, impossible de créer un compte seafile (cookie absent)".format(nouveau_mail), file=sys.stderr)
            try:
                self.seaf.migrate_account(ancien_mail, nouveau_mail)
                self.seaf.delete_user(ancien_mail)
            except NotFound:
                # si le compte n'est pas trouvé, on abandonne
                return
        
            if ANNEXE_MIGRATION:
                subprocess.check_output(
                    [ANNEXE_MIGRATION, ancien_mail, nouveau_mail]
                )
        else:
            print('Seaf10: Création du compte seafile {}'.format(nouveau_mail))
            print('Seaf10: Migration du compte seafile {} vers {}'.format(ancien_mail, nouveau_mail))
            print('Seaf10: Suppression du compte seafile {}'.format(ancien_mail))
            
            if ANNEXE_MIGRATION:
                print('Lancement des scripts annexes de migration de {} vers {}'.format(ancien_mail, nouveau_mail))
    
    def _migration_mail_seaf11(self, ancien_mail, nouveau_mail, galilee_uid, dry_run=False):
        """Change le mail identifiant un compte Seafile
        
        Si un script ANNEXE_MIGRATION est défini dans la configuration, le lance
        
        (uniquement pour Seafile v11+)
        """
        if conf.FORCE_DRY_RUN:
            dry_run = True
        
        if ancien_mail.lower() == nouveau_mail.lower():
            # vérification que les deux identifiants sont bien différents…
            return
        
        if not dry_run:
            mail_id = self.seaf_sso.get_seaf_id_ldap(ancien_mail)
            self.seaf_sso.change_auth_ldap(mail_id, nouveau_mail)
            
            if ANNEXE_MIGRATION:
                subprocess.check_output(
                    [ANNEXE_MIGRATION, ancien_mail, nouveau_mail]
                )
        else:
            print('Seaf11: changement identifiant LDAP de {} vers {}'.format(ancien_mail, nouveau_mail))
            
            if ANNEXE_MIGRATION:
                print('Lancement des scripts annexes de migration de {} vers {}'.format(ancien_mail, nouveau_mail))

    def migration_ldap_vers_oidc(self, mail_pour_gmail=False, dry_run=False):
        """Migre toustes les utilisateurices de Seafile de LDAP vers OpenIDConnect
        
        Au passage, fusionne les comptes doublons LDAP.
        
        Si mail_pour_gmail=True, ne fait que renvoyer le mail d'info aux
        utilisateurices de gmail
        """
        if conf.FORCE_DRY_RUN:
            dry_run = True
            
                    
        subject = "[EEDF Galilée] Changement d'identifiant de connexion Seafile"
        
        mail_content = """Bonjour,
(ceci est un mail automatique)

Nous venons de changer le mode de connexion à Seafile sur Galilée, désormais
le même identifiant va être utilisé sur tous les services de Galilée
(avant Seafile était à part).

Maintenant, pour te connecter à Seafile, il faut que tu utilise ton identifiant
Galilée, qui est :
    
        {}

Ton mot de passe est toujours le même.

La manière de se connecter à Seafile pour le logiciel sur ordi et l'appli
smartphone a également changée, il faut désormais utiliser le mode
« Authentification unique ». N'hésite pas à suivre la documentation :

        https://galilee.eedf.fr/seafile/help/install_sync/

Si tu as des questions, n'hésite pas à les poser à l'équipe de bénévoles qui
gèrent Galilée, l'InterComCom : intercomcom@md2t.eu

Éclaireusement,
--
Les bénévoles de l'InterComCom
"""

        
        # récupérer la liste de toustes les utilisateurices de Galilée
        numadh_list = self.gldap.recherche_numadh('uid=*')

        # pour chaque utilisateurice du LDAP: 
        for numadh in numadh_list:
            print("Migration de #{}…".format(numadh))
            # récupère ses mails et son uid
            user_data = self.gldap.adh_data(numadh, attrs=['mail', 'uid'])
            try:
                mail_list = user_data['mail']
                uid = user_data['uid'][0]
            except KeyError:
                continue # pas de mail -> on annule
            
            if mail_pour_gmail:
                main_mail = mail_list[0]
                if main_mail.endswith('@gmail.com'):
                    # renvoie le mail d'info
                    # envoi un mail aux utilisateurices
                    content = mail_content.format(uid)
                    self._sendmail(conf.FROM_MAIL, main_mail, subject, content, dry_run)
                    print("Mail d'info renvoyé à {}".format(main_mail))
                continue
            
            # récupérer ses identifiants seafile
            seaf_id_list = []
            for mail in mail_list:
                seaf_id = self.seaf_sso.get_seaf_id_ldap(mail)
                if seaf_id:
                    seaf_id_list.append(seaf_id)
            
            # si pas d'identifiant seafile, on arrête là pour cet utilisateurice
            if not seaf_id_list:
                continue
            
            main_seaf_id = seaf_id_list[0]
            main_mail = mail_list[0]
            
            print("\tMail={} ; SeafID={} ; uid={}".format(main_mail, main_seaf_id, uid))
            
            # pour chaque seaf_id sauf le premier :
            for seaf_id in seaf_id_list[1:]:
                if not dry_run:
                    try:
                        # fusionne ce compte avec le premier
                        self.seaf.migrate_account(seaf_id, main_seaf_id)
                        # supprime ce compte
                        self.seaf.delete_user(seaf_id)
                    except NotFound:
                        # si le compte n'est pas trouvé, on abandonne
                        pass
                else:
                    print("Migrer {} vers {}".format(seaf_id, main_seaf_id))
                    print("Supprimer {}".format(seaf_id))
                print("\tFusion du compte doublon {}".format(seaf_id))
            
            # switch le premier compte vers oidc
            if not dry_run:
                self.seaf_sso.change_auth_oidc(main_seaf_id, uid)
            else:
                print("Changement SSO vers OpenIDConnect pour {} (uid={})".format(main_seaf_id, uid))
            
            # envoi un mail aux utilisateurices
            content = mail_content.format(uid)
            self._sendmail(conf.FROM_MAIL, main_mail, subject, content, dry_run)
            print("\tMigration finie.\n")

    def _sendmail(self, mail_from, mail_to, subject, content, dry_run=False):
        """Envoi d'un mail en utf-8"""
        # Thanks to https://petermolnar.net/article/not-mime-email-python-3/
        for ill in [ "\n", "\r" ]:
            subject = subject.replace(ill, ' ')
        
        headers = {
            'Content-Type': 'text; charset=utf-8',
            'Content-Disposition': 'inline',
            'Content-Transfer-Encoding': '8bit',
            'From': mail_from,
            'To': mail_to,
            'Date': datetime.datetime.now().strftime('%a, %d %b %Y  %H:%M:%S %Z'),
            'X-Mailer': 'python/galilee_seafile_ldap',
            'Subject': subject
        }
    
        
        # create the message
        msg = ''
        for key, value in headers.items():
            msg += "%s: %s\n" % (key, value)
        # add contents
        msg += "\n%s\n"  % (content)
            
        if dry_run or DONT_SEND_MAIL:
            print(msg)
        else:
            s = smtplib.SMTP("localhost")
            s.sendmail(headers['From'], headers['To'], msg.encode("utf8"))
            s.quit()
        

class SeafileSSO:
    """Classe de manipulation des SSO de Seafile"""
    
    def __init__(self):
        """Initialise la gestion des SSO en récupérant les info de BDD de Seafile"""
        if conf.FORCE_DRY_RUN:
            self.db = None
        else:
            self.db = seafile_db.SeafileDB(
                conf.CCNET_DBNAME,
                conf.SEAHUB_DBNAME,
                conf.DB_USER,
                conf.DB_PASSWORD,
                conf.DB_HOST,
                int(conf.DB_PORT),
            )
    
    def auth_info(self, mail_id):
        """Renvoi les infos d'authentification de `mail_id`"""
        try:
            return self.db.auth_info(mail_id)
        except AttributeError: # DRY_RUN
            return None

    def get_seaf_id_ldap(self, ldap_mail):
        """Renvoi le `mail_id` Seafile d'un mail ldap"""
        try:
            return self.db.get_seaf_id_by_sso(conf.LDAP_SSO_ID, ldap_mail)
        except AttributeError: # DRY_RUN
            return None
    
    def get_seaf_id_oidc(self, uid):
        """Renvoi le `mail_id` Seafile d'un uid OIDC"""
        try:
            return self.db.get_seaf_id_by_sso(conf.OIDC_SSO_ID, uid)
        except AttributeError: # DRY_RUN
            return None

    def change_auth_native(self, mail_id):
        """Change le mode d'authentification vers l'authentification native Seafile
        
        Le mot de passe du compte doit ensuite être ré-initialisé manuellement
        """
        try:
            self.db.change_auth(mail_id)
        except AttributeError: # DRY_RUN
            return

    def change_auth_ldap(self, mail_id, ldap_mail):
        """Change le mode d'authentification vers LDAP, ou l'identifiant LDAP"""
        # s'assure que le compte n'est pas en mode "forcer le changement de mdp"
        try:
            self.db.remove_force_password_change(mail_id)
            self.db.change_auth(mail_id, conf.LDAP_SSO_ID, ldap_mail)
        except AttributeError: # DRY_RUN
            return
    
    def change_auth_oidc(self, mail_id, uid):
        """Change le mode d'authenticate vers OIDC, ou l'identifiant OIDC"""
        # s'assure que le compte n'est pas en mode "forcer le changement de mdp"
        try:
            self.db.remove_force_password_change(mail_id)
            self.db.change_auth(mail_id, conf.OIDC_SSO_ID, uid)
        except AttributeError: # DRY_RUN
            return
        except pymysql.err.IntegrityError as e:
            print("ERREUR : ", e, file=sys.stderr)

    def close(self):
        """Ferme la connexion à la base de données"""
        try:
            self.db.close()
        except AttributeError: # DRY_RUN
            return
   
    def fix_ldap(self, dry_run=False, verbose=True):
        """Pour chaque utilisateurice, corriger les identifiants LDAP
            (suite à mise à jour Seafile 11)
        """
        if self.db is None:
            return
        gldap = galilee_ldap.GalileeLDAP()
        # récupérer la liste des adhérent·es concerné·es
        numadh_list = gldap.recherche_numadh('uid=*')

        # leur créer des groupes
        for numadh in numadh_list:
            # récupérer les identifiants de numadh
            user_data = gldap.adh_data(numadh, attrs=['mail', 'uid'])
            try:
                mail_list = user_data['mail']
                uid = user_data['uid'][0]
            except KeyError:
                continue # pas de mail -> on annule
            
            # Pour chaque mail (en considérant que ce mail est encore 
            # un identifiant Seafile):
            for mail in mail_list:
                # fixer l'identifiant LDAP
                if not dry_run:
                    self.change_auth_ldap(mail, mail)
                if verbose:
                    print("Compte Seafile {} -> identifiant LDAP {}".format(mail, mail))
            
