# galilee-ldap

Bibliothèques pour gérer des opérations sur le LDAP de Galilée.
Inclus la bibliothèque galilee_seafile_ldap pour la synchronisation entre
le LDAP de Galilée et les groupes Seafile

Inclus également le programme galilee_gestion_ldap.py pour les opérations
courantes de gestion et synchronisation du LDAP de Galilée

## installation :

Dépendances :

 * python3-ldap
 * python3-passlib
 * python3-argcomplete
 * python3-jinja2
 * python3-prettytable
 * python_seafile_webapi https://framagit.org/florencebiree/python-seafile-webapi

Le fichier conf.py.sample est à placer dans /etc/galilee_ldap/conf.py
Le fichier galilee_seafile_conf.py.sample est à placer dans /etc/galilee_ldap/galilee_seafile_conf.py

Attention aux droits de lecture, qui doivent être restreints !
Y compris de la base de données des suspensions/exclusions.

## Dévelopement

Le shell ./galilee_ldap_shell.py permet de tester des opérations de la 
bibliothèque.

On peut ainsi :

 1. tester la bibliothèque localement, avec un LDAP distant. Le script 
    ./local-ldap.sh permet de se connecter via SSH à un LDAP distant.
    Il faut pour cela avoir un fichier de configuration contenant le mot de
    passe du LDAP, et y ajouter le port local du LDAP (9389).

 2. utiliser la commande json_dum de ce shell pour créer un export de certaines
    données dans un fichier de test, pour pouvoir avoir un faux LDAP local.
    
 3. utiliser la bibliothèque galilee_ldap_fake.py à la place de galilee_ldap.py
    pour pouvoir développer localement sans accès au LDAP. Cette bibliothèque
    a besoin d'un jeu de donnée json.
