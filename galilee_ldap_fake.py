# -*- coding: utf-8 -*-
###############################################################################
#       galilee_ldap_fake.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Fausse bibliothèque d'appel au LDAP de Galilée

Cette bibliothèque propose à des fins de test et développement la même API que
galile_ldap.py, mais ne se connecte pas à un LDAP. Elle utilise un dump json
créé par galile_ldap.py comme base de donnée.
"""

import os
import sys
import json
from passlib.hash import ldap_salted_sha1 

# import de la configuration
sys.path.insert(0, '/etc/galilee_ldap')
import conf


# exceptions
class NumAdhInconnu(Exception):
    """Le numéro d'adhérent·e précisé est inconnu"""
    def __init__(self, numadh):
        Exception.__init__(self)
        self.numadh = numadh

class StructureInconnue(Exception):
    """Cette opération nécessite qu'une structure soit définie"""
    pass

# classe principale d'accès au faux LDAP
class FakeGalileeLDAP:
    """Gestion de simil données LDAP d'une région ou structure de Galilée
    
    structure est l'identifiant de la région/structure concernée
    Attention : cet identifiant doit correspondre à la fois au nom
    dans le ldap, mais aussi au nom de domain en .eedf.fr pour les mails
    
    structure peut être '*', pour accéder aux données de toutes les structures.
    """
    
    NumAdhInconnu = NumAdhInconnu
    StructureInconnue = StructureInconnue
    
    def __init__(self, json_file, structure='*'):
        """Chargement des données depuis le fichier json"""
        self.structure = structure
        if structure == '*':
            self.filter_structure = ''
        else:
            self.filter_structure = 'ou={},'.format(structure)
            
        self.json_file = json_file
        with open(self.json_file, 'r') as json_fd:
            self.data = json.load(json_fd)
        
    def dn(self, numadh):
        """Retourne le dn de l'adhérent·e `numadh`"""
        numadh = '{}'.format(numadh)
        for fiche in self.data:
            if fiche[1]['cn'][0] == numadh:
                return fiche[0]
        raise NumAdhInconnu(numadh)
    
    def adh_data(self, numadh, attrs=None):
        """Retourne les données de l'adhérent·e `numadh`
            Si attrs est une liste d'attributs, ne renvoie que ces attributs
        """
        if isinstance(numadh, str):
            numadh = int(numadh)
        numadh = '{:06d}'.format(numadh)
        for fiche in self.data:
            if fiche[1]['cn'][0] == numadh:
                if attrs :
                    d = {}
                    for att in fiche[1]:
                        if att in attrs:
                            d[att] = fiche[1][att]
                    return d
                else:
                    return fiche[1] 
        raise NumAdhInconnu(numadh)

    def recherche_numadh(self, filtre):
        '''Renvoie les numéros d'adhérent·es correspondants au filtre.
        
        Exemple de filtre : (&(mail=test@example.com)(uid=test))
        
        Attention : cette fonction n'est pas complète, et ne gère que des
        filtres tel que l'exemple !
        '''
        # décoder le filtre TODO: filtre complexes ?
        def decoder_filtre(filtre):
            if filtre.startswith('('):
                filtre = filtre[1:]
            if filtre.endswith(')'):
                filtre = filtre[:-1]
            
            if filtre.startswith('&'):
                filtre = filtre[1:]
                return [decoder_filtre(f)[0] for f in filtre.split(')') if f]
            else:
                return [filtre.split('=')]
        
        dec_filtre = decoder_filtre(filtre)
        
        results = []
        for fiche in self.data:
            matching = True
            for att, val in dec_filtre:
                try:
                    if fiche[1][att][0] != val:
                        matching = False
                except KeyError:
                    matching = False
            if matching:
                results.append(fiche[1]['cn'][0])
        return results
        
    def creer_compte(self, numadh, login, pw, hashed=False):
        '''Créer un compte UNIX (et plus encore)
        numadh : le numéro de l'adhérent concerné
        login : le pseudonyme choisi
        pw : mot de passe
        hashed: si le mot de passe est déjà hashé (via la fonction make_secret).
                si ce n'est pas le cas, le mot de passe est hashé avant 
                insertion.
        '''
        def _get_ou_from_dn(dn):
            ou = [p for p in dn.split(',') if p.startswith('ou=')][0]
            return ou[3:]
        # récupérer infos sur l'adhérent·e
        moddn = self.dn(numadh)
        structure = _get_ou_from_dn(moddn)
        mail_domain = conf.STRUCTURES[structure][0]
        
        # check password
        if not hashed:
            pw = self.make_secret(pw)
        
        # find the right dn
        for fiche in self.data:
            if fiche[0] == moddn:
                fiche[1]['objectClass'] = ['posixAccount']
                fiche[1]['loginShell'] = ['/bin/bash']
                fiche[1]['homeDirectory'] = ['/home/{}'.format(login)]
                fiche[1]['uidNumber'] = ['{}'.format(numadh)]
                fiche[1]['gidNumber'] = ['{}'.format(numadh)]
                fiche[1]['uid'] = [login]
                fiche[1]['userPassword'] = [pw]
                if 'mail' in fiche[1]:
                    fiche[1]['mail'].append('{}@{}'.format(login, mail_domain))
                else:
                    fiche[1]['mail'] = ['{}@{}'.format(login, mail_domain)]
        
        # enregistrer les données sur le disque
        self._save()
        
    def creer_compte_ext(self, nom, prenom, fonction, fake_numadh, mail,
            login, pw, hashed=False):
        """Créer une nouvelle entrée dans le LDAP associée à un compte UNIX,
            pour des personnes non-adhérent·es aux structures membres de
            Galilée.
            
            fake_numadh est un faux numéro d'adhérent·e, qui doit être unique.
                (aucune vérification n'est effectuée dans cette méthode)
            
            hashed: définit si le mot de passe est déjà hashé (via make_secret).
                si hashed=False, le mot de passe est hashé avant insertion.
        """
        # check password
        if not hashed:
            pw = self.make_secret(pw)
        
        # création du nouvel enregistrement
        guest={
            'objectClass': [
                'person',
                'organizationalPerson',
                'inetOrgPerson',
                'schacPersonalCharacteristics',
                'posixAccount', 
                'eedfAdherent'
            ],
            'cn': [fake_numadh],
            'sn': [nom],
            'givenName': [prenom],
            'title': [fonction],
            'mail': [mail],
            'uid': [login],
            'userPassword': [pw],
            'uidNumber': [fake_numadh],
            'gidNumber': [fake_numadh],
            'homeDirectory': ['/home/'+login],
            'loginShell': ['/bin/bash'],
        }
        
        
        dn = 'cn={fake_numadh},ou={guest_ou},{bdn}'.format(
            fake_numadh = fake_numadh,
            guest_ou = conf.GUEST_OU,
            bdn = conf.LDAP_BDN
        )
        
        # ajouter la nouvelle entrée
        self.data.append((dn, guest))
        
        # enregistrer sur le disque
        self._save()
        
    def editer_adh(self, numadh, data):
        """Modifier une entrée 
        numadh : numéro d'adhérent·e dont l'entrée est à modifier
        data : [(nom_attribut, nouvelle_valeur),...]
            nouvelle_valeur peut être None pour supprimer l'attribut.
        """
        # normaliser le numéro d'adhérent·es
        if isinstance(numadh, str):
            numadh = int(numadh)
        numadh = '{:06d}'.format(numadh)
        
        # récupérer les infos sur l'adhérent·e
        dn = self.dn(numadh)
        # effectuer les modifications
        for fiche in self.data:
            if fiche[0] == dn:
                for key in data:
                    if key[1] is None:
                        del fiche[1][key[0]]
                    else:
                        if isinstance(key[1], list):
                            fiche[1][key[0]] = key[1]
                        else:
                            fiche[1][key[0]] = [key[1]]
        # enregistrer les données sur le disque
        self._save()
    
    def dump(self, numadh_list, json_file):
        """Sauvegarde en json les données des adhérents de `numadh_list`
        
        This is an dummy function that does nothing -- only for galilee_ldap
        compatibility
        """
        pass


    ### Utilities

    def _bytes_to_unicode(self, obj):
        """Retourne une représentation unicode des bytes de obj"""
        if isinstance(obj, list):
            return [ self._bytes_to_unicode(i) for i in obj ]
        elif isinstance(obj, tuple):
            return tuple(( self._bytes_to_unicode(i) for i in obj ))
        elif isinstance(obj, dict):
            newdict = {}
            for key in obj:
                newdict[key] = self._bytes_to_unicode(obj[key])
            return newdict
        elif isinstance(obj, bytes):
            return obj.decode('utf-8')
        else:
            return obj

    def _unicode_to_bytes(self, obj):
        """Retourne une représentation bytes des textes de obj"""
        if isinstance(obj, list):
            return [ self._unicode_to_bytes(i) for i in obj ]
        elif isinstance(obj, tuple):
            return tuple(( self._unicode_to_bytes(i) for i in obj ))
        elif isinstance(obj, dict):
            newdict = {}
            for key in obj:
                newdict[key] = self._unicode_to_bytes(obj[key])
            return newdict
        elif isinstance(obj, str):
            return obj.encode('utf-8')
        else:
            return obj
    
    def make_secret(self, password):                                                       
        """                                                                          
        Encodes the given password as a base64 SSHA hash+salt buffer                 
        """
        return ldap_salted_sha1.hash(password)
        
    def _save(self):
        """Save json data to the disk"""
        with open(self.json_file, 'w') as json_fd:
            json_fd.write(json.dumps(self.data))
