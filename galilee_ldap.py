# -*- coding: utf-8 -*-
###############################################################################
#       galilee_ldap.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bibliothèque d'appel au LDAP de Galilée"""

import os
import sys
import ldap
import json
import subprocess
import datetime
from passlib.hash import ldap_salted_sha1
from ldap.filter import escape_filter_chars
from ldap.modlist import modifyModlist
from galilee_jeito import JeitoAPI, ADH_STATUS, ALERTE_ADH_STATUS, GestionAlertes

# import de la configuration
sys.path.insert(0, '/etc/galilee_ldap')
import conf

# exceptions
class NumAdhInconnu(Exception):
    """Le numéro d'adhérent·e précisé est inconnu"""
    def __init__(self, numadh):
        Exception.__init__(self)
        self.numadh = numadh

class StructureInconnue(Exception):
    """Cette opération nécessite qu'une structure soit définie"""
    pass

class DoublonNonFusionnable(Exception):
    """Le doublon n'est pas fusionnable"""
    def __init__(self, doublon, raison):
        self.doublon = doublon
        self.raison = raison

class Doublon:
    """Classe représentant un·e adhérent·e ayant plusieurs fiches dans le LDAP
    
    attributs :
    
        `dn_origine`: uniquement lié à la stratégie de recherche
        `dn_a_garder`: `dn` sélectionné selon les différentes stratégie
        `dn_a_fusionner`: liste des `dn` à fusionner/supprimer
        `tous_les_dn` : liste de tous les `dn` concernés (toujours égal à
            [`dn_a_garder`] + `dn_a_fusionner
        `motif` : explication de pourquoi ces doublons
        `certitude` : booléen indiquant la certitude de la stratégie, peut être
            'fuzzy' si certitude, mais issu d'une recherche fuzzy
    """
    
    def __init__(self):
        """Crée un nouveau doublon"""
        self.dn_origine = None
        self.dn_a_garder = None
        self.dn_a_fusionner = []
        self.motif = None
        self.certitude = False
        
    @property
    def tous_les_dn(self): 
        """Tous les `dn` concernés par le doublon""" 
        if self.dn_a_garder:
            return [self.dn_a_garder] + self.dn_a_fusionner
        else:
            return self.dn_a_fusionner
    
    def __repr__(self):
        """Représentation du doublon"""
        def _cert():
            if self.certitude == 'fuzzy':
                return '(approx)'
            elif self.certitude:
                return '(sûr)'
            else:
                return '(non sûr)'
        if self.dn_a_garder:
            return "D<{}>{} {} {}".format(
                self.dn_a_garder,
                self.dn_a_fusionner,
                self.motif,
                _cert(),
            )
        else:
            return "D{} {} {}".format(
                self.dn_a_fusionner,
                self.motif,
                _cert(),
            )
    
    def infos(self, galilee_ldap):
        """Affiche les infos sur le doublon et la proposition de fusion
        
        galilee_ldap est une instance de GalileeLDAP pour récupérer les infos
        des fiches adhérent·es
        """
        def _format_details(dn_details, dn):
            return """\
                  ldap dn : {dn}
       prénom (givenName) : {givenName}
                 nom (sn) : {sn}
   addresses mails (mail) : {mail}
             num adh (cn) : {cn}
saison (membershipSeason) : {membershipSeason}
           compte galilée : {cpt}
 nom utilisateurice (uid) : {uid}
 """.format(**{
                'dn': dn,
                'givenName': dn_details[dn].get('givenName', [''])[0],
                'sn': dn_details[dn].get('sn', [''])[0],
                'mail': dn_details[dn].get('mail', ''),
                'cn': dn_details[dn].get('cn', [''])[0],
                'membershipSeason': dn_details[dn].get('membershipSeason', [''])[0],
                'cpt': 'oui' if 'uid' in dn_details[dn] else 'non',
                'uid': dn_details[dn].get('uid', [''])[0],
            })
        
        dn_details = {}
        for dn in self.tous_les_dn:
            data = galilee_ldap._bytes_to_unicode(galilee_ldap.ldap_connexion.search_s(
                dn,
                ldap.SCOPE_SUBTREE,  
                '(objectClass=person)',
                ['cn', 'sn', 'givenName', 'mail', 'membershipSeason', 'uid']  
            ))
            dn_details[dn] = data[0][1]
                
        msg = ""
        if self.certitude:
            msg += "Fusion certaine, raison : {}\n".format(self.motif)
            msg += "Fiche à garder (et à y transférer les infos de compte Galilée) :\n"
            msg += _format_details(dn_details, self.dn_a_garder)
            msg += "\n"
            msg += "Fiches à supprimer (en déplaçant les infos de compte Galilée) :\n"
            for dn in self.dn_a_fusionner:
                msg += _format_details(dn_details, dn)
                msg += '\n'
        else:
            msg += "Doublon sans stratégie de fusion, raison : {}\n".format(self.motif)
            msg += "Fiches en doublon (pas de stratégie de fusion certaine) :\n"
            for dn in self.tous_les_dn:
                msg += _format_details(dn_details, dn)
                msg += '\n'
        return msg

# classe principale d'accès au LDAP
class GalileeLDAP:
    """Gestion des données LDAP d'une région ou structure de Galilée
    
    structure est l'identifiant de la région/structure concernée
    Attention : cet identifiant doit correspondre à la fois au nom
    dans le ldap, mais aussi au nom de domain en .eedf.fr pour les mails
    
    structure peut être '*', pour accéder aux données de toutes les structures.
    """
    
    NumAdhInconnu = NumAdhInconnu                                                
    StructureInconnue = StructureInconnue
    
    def __init__(self, structure='*'):
        """Connexion en root au serveur LDAP"""
        self.ldap_connexion = ldap.initialize(conf.LDAP_URL)
        self.ldap_connexion.simple_bind_s(conf.LDAP_RDN, conf.LDAP_PW)
        
        self.structure = structure
        if structure == '*':
            self.filter_structure = ''
        else:
            self.filter_structure = 'ou={},'.format(structure)
        
        #self.alerte_adh = [] # liste de tuple (cn, status)
        try:
            self.gest_alertes = GestionAlertes()
        except PermissionError:
            self.gest_alertes = None # pas d'accès au fichier, alertes désactivées
        
        self.bdn = conf.LDAP_BDN
            
    def normalize_num_adh(self, numadh):
        """Renvoi le numéro d'adhérent sous forme de str normalisé
            (si moins de 6 chiffres -> 6 chiffres avec des zéros devant)
            accepte les numadh `SAL-`
        """
        try:
            return '{:06d}'.format(int(numadh))
        except ValueError:
            return numadh
        
    def dn(self, numadh):
        """Retourne le dn de l'adhérent·e `numadh`"""
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN, 
            ldap.SCOPE_SUBTREE, 
            'cn={}'.format(self.normalize_num_adh(numadh)), 
            ['cn'] 
        )
        if data:
            return data[0][0] # we assume results are uniq…
        else:
            return None

    def all(self):
        """Renvoie toutes les données du périmètre concerné"""
        return self._bytes_to_unicode(
            self.ldap_connexion.search_s(
                self.filter_structure + conf.LDAP_BDN,
                ldap.SCOPE_SUBTREE,
            )
        )

    def adh_data(self, numadh, attrs=None):
        """Retourne les données de l'adhérent·e `numadh`
            Si attrs est une liste d'attributs, ne renvoie que ces attributs
        """
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN,
            ldap.SCOPE_SUBTREE,
            'cn={}'.format(self.normalize_num_adh(numadh)),
            attrs
        )
        if data:
            return self._bytes_to_unicode(data[0][1])
        else:
            raise NumAdhInconnu(numadh)

    def recherche_numadh(self, filtre):
        '''Renvoie les numéros d'adhérent·es correspondants au filtre.
        
        Exemple de filtre : (mail=test@example.com,uid=test)
        ''' 
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN,
            ldap.SCOPE_SUBTREE,
            filtre,
            ['cn']
        )
        if data:
            u_data = []
            for res in data:
                try:
                    u_data.append(self._bytes_to_unicode(res[1]['cn'][0]))
                except KeyError:
                    pass #no cn… we forget
            return u_data
        else:
            return []
        
    def creer_compte(self, numadh, login, pw, hashed=False):
        '''Créer un compte UNIX (et plus encore)
        numadh : le numéro de l'adhérent concerné
        login : le pseudonyme choisi
        pw : mot de passe
        hashed: si le mot de passe est déjà hashé (via la fonction make_secret).
                si ce n'est pas le cas, le mot de passe est hashé avant 
                insertion.
        '''
        def _get_ou_from_dn(dn):
            ou = [p for p in dn.split(',') if p.startswith('ou=')][0]
            return ou[3:]
        # récupérer infos sur l'adhérent·e
        numadh = self.normalize_num_adh(numadh)
        moddn = self.dn(numadh)
        structure = _get_ou_from_dn(moddn)
        try:
            mail_domain = conf.STRUCTURES[structure][0]
        except KeyError:
            # si structure non trouvée, on mets un mail comme guest
            # concerne notamment des comptes « futureaaee ».
            mail_domain = conf.STRUCTURES[conf.GUEST_OU][0]
        
        # check password
        if not hashed:
            pw = self.make_secret(pw)
        
        # Ajouter les attributs du compte
        mod = [
            (ldap.MOD_ADD, 'objectClass', 'posixAccount'), 
            (ldap.MOD_ADD, 'loginShell', '/bin/bash'),
            (ldap.MOD_ADD, 'homeDirectory','/home/'+login),
            (ldap.MOD_ADD, 'uidNumber', '{}'.format(self._makeUidNumber(numadh))),
            (ldap.MOD_ADD, 'gidNumber', '{}'.format(self._makeUidNumber(numadh))),
            (ldap.MOD_ADD, 'uid', login),
            (ldap.MOD_ADD, 'userPassword', pw),
            (ldap.MOD_ADD, 'mail', '{}@{}'.format(login, mail_domain)),
        ]
        mod = [
            (op, at, self._unicode_to_bytes(val)) for (op, at, val) in mod
        ]
        try:
            self.ldap_connexion.modify_s(moddn, mod)
        except ldap.CONSTRAINT_VIOLATION:
            return False
        else:
            return True
    
    def creer_compte_ext(self, nom, prenom, fonction, fake_numadh, mail,
            login, pw, hashed=False):
        """Créer une nouvelle entrée dans le LDAP associée à un compte UNIX,
            pour des personnes non-adhérent·es aux structures membres de
            Galilée.
            
            fake_numadh est un faux numéro d'adhérent·e, qui doit être unique.
                (aucune vérification n'est effectuée dans cette méthode)
            
            hashed: définit si le mot de passe est déjà hashé (via make_secret).
                si hashed=False, le mot de passe est hashé avant insertion.
        """
        # check password
        if not hashed:
            pw = self.make_secret(pw)
        
        # création du nouvel enregistrement
        guest=[
            ('objectClass',[
                'person',
                'organizationalPerson',
                'inetOrgPerson',
                'schacPersonalCharacteristics',
                'posixAccount', 
                'eedfAdherent'
            ]),
            ('cn', fake_numadh),
            ('sn', nom),
            ('givenName', prenom),
            ('title', fonction),
            ('mail', [mail, '{}@{}'.format(login, conf.DOMAINE_MAIL)]),
            ('uid', login),
            ('userPassword', pw),
            ('uidNumber', fake_numadh),
            ('gidNumber', fake_numadh),
            ('homeDirectory', '/home/'+login),
            ('loginShell', '/bin/bash'),
            ('displayName', '{} {}'.format( prenom.title(), nom.title() ) ),
        ]
        guest = [
            (at, self._unicode_to_bytes(val)) for (at, val) in guest
        ]
        
        dn = 'cn={fake_numadh},ou={guest_ou},{bdn}'.format(
            fake_numadh = fake_numadh,
            guest_ou = conf.GUEST_OU,
            bdn = conf.LDAP_BDN
        )
        
        # ajouter la nouvelle entrée dans le LDAP
        self.ldap_connexion.add_s(dn, guest)

    def editer_adh(self, numadh, data):
        """Modifier une entrée 
        numadh : numéro d'adhérent·e dont l'entrée est à modifier
        data : [(nom_attribut, nouvelle_valeur),...]
            nouvelle_valeur peut être None pour supprimer l'attribut.
        """
        # normaliser le numéro d'adhérent·es
        numadh = self.normalize_num_adh(numadh)
        
        mod = [] 
        for key in data: 
            if key[1] is None: 
                op = ldap.MOD_DELETE
            else:
                op = ldap.MOD_REPLACE 
            mod.append((op, key[0], self._unicode_to_bytes(key[1])))
        self.ldap_connexion.modify_s(self.dn(numadh), mod)
    
    def desactiver_cpt(self, numadh):
        """Désactiver l'accès au compte `numadh`. Aucune donnée perdue, si
            ce n'est le mot de passe."""
        self.editer_adh(numadh, [('userPassword', '{SSHA}!')])
    
    def reactiver_cpt(self, numadh):
        """Ré-activer l'accès au compte `numadh`. Le mot de passe devra être
        re-défini par la personne elle-même."""
        self.editer_adh(numadh, [('userPassword', '{SSHA}')])
    
    def dump(self, numadh_list, json_file):
        """Sauvegarde en json les données des adhérents de `numadh_list`"""
        data = []
        for numadh in numadh_list:
            data.append((
                self.dn(numadh),
                self.adh_data(numadh)
            ))
        with open(json_file, 'w') as json_fd:
            json_fd.write(json.dumps(data))

    # gestion des adhérent·es (doublons et fusions)
    
    def toustes_les_adherentes(self, compte_uniquement=False, structure=None):
        '''Renvoie les dn de toustes les adhérent·es de l'asso
        si `compte_uniquement`, ne renvoie que les utilisateurices ayant
        un compte Galilée
        si `structure`, ne renvoie que les résultats de ou=`structure`
        '''
        if compte_uniquement:
            filtre = '(&(objectClass=person)(uid=*))'
        else:
            filtre = '(objectClass=person)'
        if structure:
            searchdn = 'ou={},{}'.format(structure, conf.LDAP_BDN)
        else:
            searchdn = self.filter_structure + conf.LDAP_BDN
        data = self.ldap_connexion.search_s(
            searchdn,
            ldap.SCOPE_SUBTREE,
            filtre,
            ['dn']
        )
        return [self._bytes_to_unicode(adh[0]) for adh in data]
        
        return data
    
    def _doublons_cn(self, dn):
        '''Rechercher des doublons de la fiche `dn` qui ont le même cn
            (même numéro d'adhésion)
            
            Renvoie un Doublon()
        '''
        # chercher des fiches de même cn (même numéro d'adhésion)
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN,
            ldap.SCOPE_SUBTREE,
            '({})'.format(dn.split(',')[0], dn),
            ['dn', 'membershipSeason', 'uid']
        )
        meme_cn = [adh[0] for adh in data if adh[0] != dn]
        membershipSeason = {}
        for adh in data:
            try:
                membershipSeason[adh[0]] = self._bytes_to_unicode(adh[1]['membershipSeason'][0])
            except KeyError:
                membershipSeason[adh[0]] = ''
    
        if not meme_cn:
            return None
        
        # on a un doublon
        dbl = Doublon()
        dbl.dn_origine = dn
        
        # cherche une stratégie de fusion
        
        # vérifier le nombre d'uid dans les fiches : 
        nb_uid = 0 
        for adh in data: 
            if 'uid' in adh[1]: 
                nb_uid += 1 
        if nb_uid > 1: 
            # plus d'un uid : besoin d'une fusion manuelle, pas de certitude de stratégie 
            dbl.dn_a_fusionner = [adh[0] for adh in data] 
            dbl.motif = "Des fiches adhérent·es d'anciennes saisons ont été gardées, mais plusieurs compte Galilée"
            dbl.certitude = False 
            return dbl
        
        # ordonner selon les saisons
        dn_ordre_saison = sorted(membershipSeason, key=lambda a:membershipSeason[a])
        
        # vérifier que les saisons sont différentes entre le dernier et l'avant
        # dernier
        if dn_ordre_saison[-1] != dn_ordre_saison[-2]:
            dbl.dn_a_garder = dn_ordre_saison[-1]
            dbl.dn_a_fusionner = dn_ordre_saison[:-1]
            dbl.motif = "Des fiches adhérent·es d'anciennes saisons ont été gardées"
            dbl.certitude = True
        else:
            dbl.dn_a_fusionner = dn_ordre_saison
            dbl.motif = "Des fiches adhérent·es d'anciennes saisons ont été gardées (mais certaines sont de la même saison)"
    
        return dbl
            
    def recherche_doublon_cn(self, compte_uniquement=False):
        '''Recherche les doublons dans le LDAP (même cn, donc même num adhérent).
        
        si `compte_uniquement`, ne cherche que les doublons parmis les fiches
        adhérent·es qui ont au moins un compte Galilée
        
        Renvoie : une liste de Doublon()
        '''
        toustes = self.toustes_les_adherentes(compte_uniquement)
        
        # recherche de cn similaire
        meme_cn = []
        dn_deja_traites = []
        for dn in toustes:
            if not dn in dn_deja_traites:
                resultat = self._doublons_cn(dn)
                if resultat:
                    meme_cn.append(resultat)
                    dn_deja_traites += resultat.tous_les_dn
        
        return meme_cn

    def _doublons_id(self, dn, fuzzy=False, just_mail=False):
        '''Rechercher des doublons de la fiche `dn` qui ont une même identité
            (même mail, puis même noms et prénoms)
            
            si `fuzzy`, fait une recherche approximative sur les noms et prénoms
            si `just_mail`, cherche les fiches qui ont le même mail (sans cherche
                une similarité sur les noms et prénoms)
            
            Renvoie un Doublon()
        '''
        # obtenir des infos sur `dn`
        data = self.ldap_connexion.search_s(
            dn,
            ldap.SCOPE_SUBTREE,
            '(objectClass=person)',
            ['mail', 'givenName', 'sn', 'membershipSeason']
        )
        dn_origin_data = self._bytes_to_unicode(data[0][1])
        dn_origin_data['sn'] = dn_origin_data.get('sn', [''])[0] # nom de famille
        dn_origin_data['givenName'] = dn_origin_data.get('givenName', [''])[0] # prénom
        
        if not 'mail' in dn_origin_data:
            # si la fiche `dn` n'a pas d'adresse mail, pas de recherche de doublon
            return None
        
        if fuzzy and not just_mail:
            f = ('(&'
                # même mail
                + '(|{})'.format(''.join('(mail={})'.format(m) for m in dn_origin_data['mail']))
                # approximativement le même prénom
                + '(givenName~={})'.format(escape_filter_chars(dn_origin_data['givenName']))  
                # approximativement le même nom 
                + '(sn~={})'.format(escape_filter_chars(dn_origin_data['sn'])) 
                + ')' 
            )
        elif just_mail:
            f = '(|{})'.format(''.join('(mail={})'.format(m) for m in dn_origin_data['mail']))
        else:
        # chercher des fiches de même mails, prénoms, noms
            f = ('(&' 
                # même mail
                + '(|{})'.format(''.join('(mail={})'.format(m) for m in dn_origin_data['mail'])) 
                # même prénom
                + '(givenName={})'.format(escape_filter_chars(dn_origin_data['givenName'])) 
                # même nom
                + '(sn={})'.format(escape_filter_chars(dn_origin_data['sn']))
                + ')'
            )
        
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN,
            ldap.SCOPE_SUBTREE,
            f,
            ['dn', 'mail', 'givenName', 'sn', 'membershipSeason', 'uid']
        )
        if len(data) <= 1:
            # pas de doublon trouvé
            return None
        
        # on a un doublon
        dbl = Doublon()
        dbl.dn_origin = dn
        
        # chercher une stratégie de fusion
        data = self._bytes_to_unicode(data)
        
        # si les noms/prénoms est similaire à la casse prête, on annule les flags
        # fuzzy et just_mail
        gn = data[0][1].get('givenName', [''])[0].lower()
        sn = data[0][1].get('sn', [''])[0].lower()
        est_similaire = True
        for adh in data[1:]:
            gn1 = adh[1].get('givenName', [''])[0].lower()
            sn1 = adh[1].get('sn', [''])[0].lower()
            if ( ( gn1 != gn) or ( sn1 != sn) ):
                est_similaire=False
        if est_similaire:
            fuzzy, just_mail = False, False
        
        # vérifier le nombre d'uid dans les fiches : 
        nb_uid = 0
        for adh in data:
            if 'uid' in adh[1]:
                nb_uid += 1
        if nb_uid == 0:
            # pas d'uid : pas d'intérêt de fusionner : c'est un problème de
            # doublon dans jeito, sans incidence sur Galilée… Les anciennes
            # données seront supprimée lors des changements de saison
            return None
        elif nb_uid > 1:
            # plus d'un uid : besoin d'une fusion manuelle, pas de certitude de stratégie
            dbl.dn_a_fusionner = [adh[0] for adh in data]
            dbl.motif = "Même mail, même prénom, même nom, mais plus que un compte Galilée"
            dbl.certitude = False
            return dbl
        
        # stratégie : si deux fiches identifiée, et l'une est dans ou=guests,
        # alors celle de guests est le doublon de l'autre
        if len(data) == 2:
            dn_in_guest = None
            good_dn = None
            if conf.GUEST_OU in data[0][0]:
                dn_in_guest = data[0][0]
                good_dn = data[1][0]
            elif conf.GUEST_OU in data[1][0]:
                dn_in_guest = data[1][0]
                good_dn = data[0][0]
            if dn_in_guest:
                dbl.dn_a_garder = good_dn
                dbl.dn_a_fusionner = [dn_in_guest]
                dbl.motif = "Fiche de non-adhérent·e à fusionner avec une fiche adhérent·e"
                if just_mail:
                    dbl.certitude = False # jamais de certitude avec just_mail
                    dbl.motif += " !! seulement mail en commun !!"
                elif not just_mail and fuzzy:
                    dbl.certitude = "fuzzy" # True, mais avec un avertissement
                else:
                    dbl.certitude = True
                return dbl

        # stratégie : garder la saison la plus récent
        # ordonner selon les saisons
        for adh in data:
            if not 'membershipSeason' in adh[1]:
                adh[1]['membershipSeason'] = ['']
        data_ordre_saison = sorted(
            data, 
            key=(lambda a:a[1]['membershipSeason'][0])
        )
        
        # vérifier que les saisons sont différentes entre le dernier et l'avant
        # dernier
        if data_ordre_saison[-1][1]['membershipSeason'][0] != data_ordre_saison[-2][1]['membershipSeason'][0]:
            dbl.dn_a_garder = data_ordre_saison[-1][0]
            dbl.dn_a_fusionner = [adh[0] for adh in data_ordre_saison[:-1]]
            dbl.motif = "Des fiches adhérent·es d'anciennes saisons ont été gardées (doublon de numéro d'adhérent·e !)"
            if just_mail:
                dbl.certitude = False # jamais de certitude avec just_mail
                dbl.motif += " !! seulement mail en commun !!"
            elif not just_mail and fuzzy: 
                dbl.certitude = "fuzzy" # True, mais avec un avertissement 
            else: 
                dbl.certitude = True 
            return dbl
        
        # sinon doublon sans stratégie
        dbl.dn_a_fusionner = [adh[0] for adh in data]
        dbl.motif = "Même mail, même prénom, même nom"
        dbl.certitude = False
        if just_mail:
            dbl.motif += " !! seulement mail en commun !!"
        return dbl
        
        
    def recherche_doublon_id(self, compte_uniquement=False, guests_uniquement=False, fuzzy=False, just_mail=False):
        '''Recherche les doublons dans le LDAP (même mail, puis même noms et prénoms)
        
        si `compte_uniquement`, ne cherche que les doublons parmis les fiches
        adhérent·es qui ont au moins un compte Galilée
        
        si `guests_uniquement`, ne cherche que les doublons parmis l'ou=guests
        
        si `fuzzy`, fait une recherche approximative sur les noms et prénoms
        
        si `just_mail`, ne cherche même pas de similarité entre les noms et prénoms
        
        Renvoie : une liste de Doublon()
        '''
        if guests_uniquement:
            toustes = self.toustes_les_adherentes(compte_uniquement, structure=conf.GUEST_OU)
        else:
            toustes = self.toustes_les_adherentes(compte_uniquement)
        # on crée d'abord des "paquets" de dn qui ont un attribut en commun
        # les dn d'un paquet sont exclus de la recherche
        
        # recherche de cn similaire
        liste_doublons = []
        dn_deja_traites = []
        for dn in toustes:
            if not dn in dn_deja_traites:
                resultat = self._doublons_id(dn, fuzzy=fuzzy, just_mail=just_mail)
                if resultat:
                    liste_doublons.append(resultat)
                    dn_deja_traites += resultat.tous_les_dn
        
        return liste_doublons

    def fusion(self, doublon, demande_avant_suppression=True):
        """Fusionne plusieurs fiches adhérent·es telles que décrite par un Doublon()
        
        Si `demande_avant_suppression`, demande interractivement avant toute suppression.
        
        Renvoie [numadh, uid, nouveau_mail, anciens_mails…] de la fiche fusionnée
        
        Si anciens_mails est le même que nouveau_mail, alors il n'y a pas d'élément
        """
        
        if not doublon.dn_a_garder:
            raise DoublonNonFusionnable(doublon, "Doublon sans stratégie de fusion")
        
        # pour chaque doublon, on récupère les infos nécessaires aux tests et à la fusion
        dn_data = {}
        for dn in doublon.tous_les_dn:
            data = self.ldap_connexion.search_s(
                dn,
                ldap.SCOPE_SUBTREE,
                '(objectClass=person)',
                ['loginShell', 'homeDirectory', 'uid', 'userPassword', 'mail', 'cn', 'uidNumber', 'gidNumber']
            )
            dn_data[dn] = self._bytes_to_unicode(data[0][1])
        
        # vérification
        dn_avec_uid = []
        for dn in dn_data:
            if 'uid' in dn_data[dn]:
                dn_avec_uid.append(dn)
        
        if len(dn_avec_uid) > 1:
            # TODO: fusionner plusieurs comptes Galilée ??
            raise DoublonNonFusionnable(doublon, "Il y a plus que un compte Galilée dans ces fiches adhérent·es")
        elif len(dn_avec_uid) == 1:
            # tri entre les anciens et nouveaux mails
            mails_uid = dn_data[dn_avec_uid[0]]['mail']
            mails_a_garder = dn_data[doublon.dn_a_garder]['mail']
            anciens_mails = []
            for mail in mails_uid:
                if not ((mail in mails_a_garder) or self._est_un_mail_galilee(mail)):
                    anciens_mails.append(mail)
                    # avant seafile v8, on refusait le changement de mail d'un compte seafile
                    #raise DoublonNonFusionnable(doublon, "Changement d'adresse mail, posera problème dans Seafile")
            
            uid_data = dn_data[dn_avec_uid[0]]
            numadh = dn_data[doublon.dn_a_garder]['cn'][0]
            uid = uid_data['uid'][0]
            
            # vérifier que l'uid n'est pas déjà sur la bonne fiche
            if doublon.dn_a_garder != dn_avec_uid[0]:
                # construction des mails à mettre (`mail_a_garder` + mail galilée s'il existe)
                
                # vérification qu'il y a bien un mail galilee associé au compte
                mail_galilee = False
                for mail in mails_uid:
                    if self._est_un_mail_galilee(mail):
                        mail_galilee = True
                if not mail_galilee:
                    mails_uid.append('{}@{}'.format(uid_data['uid'][0], conf.DOMAINE_MAIL))
                
                # copier l'uid/etc sur la fiche à garder
                # ne garder que les mails galilée présent dans l'ancienne fiche
                new_mails = [
                    mail for mail in mails_uid 
                    if ( (mail not in mails_a_garder) and self._est_un_mail_galilee(mail) )
                ]
                homedir = uid_data['homeDirectory'][0]
                
                mod = [
                    (ldap.MOD_ADD, 'objectClass','posixAccount'),
                    (ldap.MOD_ADD, 'loginShell', uid_data['loginShell'][0]),
                    (ldap.MOD_ADD, 'homeDirectory', homedir),
                    (ldap.MOD_ADD, 'uidNumber', '{}'.format(self._makeUidNumber(numadh))),
                    (ldap.MOD_ADD, 'gidNumber', '{}'.format(self._makeUidNumber(numadh))),
                    (ldap.MOD_ADD, 'uid', uid),
                    (ldap.MOD_ADD, 'userPassword', uid_data['userPassword'][0]),
                ] + [
                    (ldap.MOD_ADD, 'mail', mail)
                    for mail in new_mails
                ]
    
                mod = [
                    (op, at, self._unicode_to_bytes(val)) for (op, at, val) in mod
                ]
                self.ldap_connexion.modify_s(doublon.dn_a_garder, mod)
                            
                # faire un chown sur /home/uid
                if os.path.isdir(homedir):
                    subprocess.check_output( 
                        ['chown', '-R', '{na}:{na}'.format(na=int(self._makeUidNumber(numadh))), homedir]
                    )
        else:
            uid = None
            mails_a_garder = dn_data[doublon.dn_a_garder]['mail']
            anciens_mails = []
        
        # supprimer les autres fiches
        if demande_avant_suppression:
            print("Les données ont bien été fusionnées dans la fiche {}".format(doublon.dn_a_garder))
        for dn in doublon.dn_a_fusionner:
            if demande_avant_suppression:
                res = input('Supprimer la fiche {} ? [o/N] '.format(dn))
                if res.lower() == 'o':
                    self.ldap_connexion.delete_s(dn)
            else:
                self.ldap_connexion.delete_s(dn)
        
        # renvoyer le numéro d'adhérent·e de la fiche garder
        # (permet la gestion des groupes seafile)
        return [
            dn_data[doublon.dn_a_garder].get('cn', [None])[0],
            uid,
            mails_a_garder[0],
        ] + anciens_mails

    def fusion_dn(self, dn_a_garder, liste_dn_a_fusionner, demande_avant_suppression=True):
        """Fusionne les `dn` de `liste_dn_a_fusionner` dans `dn_a_garder`
        
        Renvoi (numadh, uid) de `dn_a_garder`.
        """
        dbl = Doublon()
        dbl.dn_origin = dn_a_garder
        dbl.dn_a_garder = dn_a_garder
        dbl.dn_a_fusionner = liste_dn_a_fusionner
        dbl.motif = "Demande manuelle de fusion"
        dbl.certitude = True
        return self.fusion(dbl, demande_avant_suppression)

    # fonctions pour le ménage annuel

    def liste_dn_a_supprimer(self, annee):
        """Liste les dn à supprimer pour le ménage annuel du LDAP :
            
            uniquement les `dn` sans uid (sans compte Galilée)
            les `dn` sans membershipSeason (très vieux `dn`)
            les `dn` des saisons jusqu'à la saison  (année-1)-année
            
            Par exemple, annee=2021 va lister les entrées jusqu'à la saison
            2020-2021.
            
            Pour le ménage d'août, si on est en 2023, il faut lancer
            liste_dn_a_supprimer(2022), pour supprimer jusqu'à la saison
            2021-2022, et garder 2022-2023, qui devient l'année n-1 en septembre
        """
        def _get_dn(filtre):
            data = self.ldap_connexion.search_s(
                self.filter_structure + conf.LDAP_BDN, 
                ldap.SCOPE_SUBTREE, 
                filtre,
                ['dn'], 
            )
            if data:
                return [adh[0] for adh in data]
            else:
                return []
        
        a_supprimer = [] # liste de (dn, membershipSeason)
        # récupérer les dn sans membershipSeason:
        a_supprimer = a_supprimer + [ 
            (dn, None) for dn in        
            _get_dn('(&(!(membershipSeason=*))(!(uid=*))(objectClass=person))')
        ]
        
        # les dn avec des saisons ni l'actuelle, ni la précédente
        # on part de 2017-2018 jusqu'à annee (fait en avril 2023 : 2020-2021 comprise)

        annee_debut = 2018
        annee_fin = int(annee)

        annee_actuelle = annee_debut
        annee_prec = annee_actuelle - 1

        while annee_actuelle <= annee_fin:
            saison = '{}-{}'.format(annee_prec, annee_actuelle)
            a_supprimer = a_supprimer + [
                (dn, saison) for dn in
                _get_dn("(&(membershipSeason={})(!(uid=*))(objectClass=person))".format(saison))
            ]
            
            saison_courte = "{ap}-{ap}".format(ap=annee_prec)
            a_supprimer = a_supprimer + [
                (dn, saison_courte) for dn in
                _get_dn("(&(membershipSeason={})(!(uid=*))(objectClass=person))".format(saison_courte))
            ]
            
            annee_actuelle += 1
            annee_prec += 1
        
        return a_supprimer

    def menage(self, list_dn):
        """Supprime les dn de `list_dn`
        
        Renvoie le nombre d'entrées supprimées
        """
        nb_suppr = 0
        for dn in list_dn:
            self.ldap_connexion.delete_s(dn)
            nb_suppr += 1
        return nb_suppr

    # fonctions pour l'import/synchronisation depuis jeito

    def synchro(self, quiet=False, seaf_mig=None, seaf_set_grp=None, dry_run=False):
        """Import des données de jeito
        
            si quiet=True, n'affiche que des messages sur stderr (dont messages
            liés aux suspension/radiations)
            
            seaf_mig: fonction à appeler pour migrer un compte seafile de mail
            seaf_set_grp: fonction à appeler pour redéfinir les groupes seafile
        
        
            sauvegarde dans self.alerte_adh des tuples (cn, status), avec
            les adhérent·es suspendu·es ou radié·es
        """
        if self.gest_alertes is None:
            print("Gestion des alertes désactivées (pas d'accès à la base de donnée des alertes ?)", file=sys.stderr)
            print("La synchro avec jeito ne peut pas avoir lieu. Annulation.", file=sys.stderr)
            return
            
        def _get_attr_values(ldap_attr_name, person):
            """Retourn les valeurs pour l'attribut
                (toujours sous forme de liste)"""
            jeito_attr_name = conf.LIST_CORR[ldap_attr_name]
            if isinstance(jeito_attr_name, list):
                list_val = []
                for j_attr in jeito_attr_name:
                    try:
                        if person[j_attr]:
                            list_val.append(str(person[j_attr]))
                    except KeyError:
                        pass
                if list_val:
                    return [", ".join(list_val)]
                else:
                    return None
            else:
                try:
                    val = person[jeito_attr_name]
                    if not val:
                        return None
                    # gestion des formats de dates
                    if ldap_attr_name in ('schacDateOfBirth', ):
                        # au vu des attributs en question, on suppose qu'on n'a pas de liste
                        # on remplace 2023-10-22 par 22102023
                        dlist = val.split('-')
                        dlist.reverse()
                        val = ''.join(dlist)
                    # normalization des e-mail (lower case)
                    if ldap_attr_name in ('mail', 'respLegauxMail' ):
                        if isinstance(val, list):
                            val = [a.lower() for a in val]
                        else:
                            val = val.lower()
                    
                    if isinstance(val, list):
                        # on passe par un set() pour supprimer les doublons
                        return list(set(str(v) for v in val))
                    else:
                        return [str(val)]
                except KeyError:
                    return None
        
        def _add_request(person):
            """Renvoie une liste de tuples ('attribut', [valeur1, valeur2…])
            à ajouter au ldap pour ajoute une personne
            """
            # ajouter les objectClass par défault :
            list_tuples = [
                ('objectClass', self._unicode_to_bytes([
                    'person',
                    'organizationalPerson',
                    'inetOrgPerson',
                    'schacPersonalCharacteristics',
                    'eedfAdherent',
                ]))
            ]
            # ajouter les autres attributs
            for ldap_attr_name in conf.LIST_CORR:
                values = self._unicode_to_bytes(_get_attr_values(ldap_attr_name, person))
                if values:
                    list_tuples.append( (ldap_attr_name, values) )
            return list_tuples  
        
        # récupérer les données de jeito
        ja = JeitoAPI()
        pers_dict = ja.adhesions(quiet=quiet)
                
        for pers_id, person in pers_dict.items():
            # n'importer que les fiches avec un cn
            if 'cn' in person:
                # normaliser le cn
                person['cn'] = self.normalize_num_adh(person['cn'])
                dn_actuel = None
                # chercher s'il y a déjà une fiche avec le même `jeitoID`
                ldap_data = self.ldap_connexion.search_s(                                 
                    self.filter_structure + conf.LDAP_BDN,                           
                    ldap.SCOPE_SUBTREE,                                              
                    '(jeitoID={})'.format(person['id']),                                                          
                    None,                                                          
                )
                if ldap_data:
                    dn_actuel = ldap_data[0][0]
                
                # sinon chercher s'il y a une fiche avec le même `cn`
                if not dn_actuel:
                    ldap_data = self.ldap_connexion.search_s(
                        self.filter_structure + conf.LDAP_BDN,
                        ldap.SCOPE_SUBTREE,
                        '(cn={})'.format(self.normalize_num_adh(person['cn'])),
                        None,
                    )
                    if ldap_data:
                        dn_actuel = ldap_data[0][0]
                
                # s'il y a une fiche, la mettre à jour
                if dn_actuel:
                    ldap_data = ldap_data[0][1]
                    jeito_data = {}
                    for key, val in _add_request(person):
                        jeito_data[key] = val
                    
                    if 'uid' in ldap_data:
                        # ajouter les mails Galilée à jeito_data
                        try:
                            for addr in ldap_data['mail']:
                                if self._est_un_mail_galilee(self._bytes_to_unicode(addr)):
                                    if not 'mail' in jeito_data:
                                        jeito_data['mail'] = [addr]
                                    elif not addr in jeito_data['mail']:
                                        jeito_data['mail'].append(addr)
                        except KeyError:
                            pass # cette fiche LDAP n'a aucun mail associé
                        
                        # ajouter la classe posixAccount
                        jeito_data['objectClass'].append(b'posixAccount')
                    
                    # calculer les changements de DN
                    # ex: cn=545270,ou=midipy,dc=galilee,dc=eedf,dc=fr vers national
                    nouveau_dn = 'cn={},ou={},{}'.format(
                        person['cn'],
                        person['ou'],
                        conf.LDAP_BDN
                    )
                    
                    # calculer les changements à faire
                    modlist = modifyModlist(
                        ldap_data,
                        jeito_data,
                        ignore_attr_types=[
                            #'objectClass',
                            'loginShell',
                            'homeDirectory',
                            'uidNumber',
                            'gidNumber',
                            'uid',
                            'userPassword'
                    ])
                    
                    if modlist:
                        new_grp = False
                        new_mail = False
                        
                        # vérifier si besoin de changement de groupes seafile ou de mail
                        if 'uid' in ldap_data:
                            # que pour les comptes Galilée
                            if set(ldap_data['mail']) != set(jeito_data.get('mail', [])):
                                new_mail = True
                                new_mail_addr = person['email']
                            if dn_actuel != nouveau_dn:
                                new_grp = True
                            if set(ldap_data['o']) != set(jeito_data['o']):
                                new_grp = True
                        
                        if not quiet: print('{} {} -> fiche à mettre à jour : {}'.format(person['cn'], person['displayName'], dn_actuel))
                        if not quiet and dn_actuel != nouveau_dn: print('\tDN à changer pour {}'.format(nouveau_dn)) 
                        if not quiet:
                            for (action, attr, val) in modlist:
                                print('\t{} {}: {}'.format(
                                    '+' if action == ldap.MOD_ADD else '-',
                                    attr,
                                    (
                                        repr(self._bytes_to_unicode(ldap_data.get(attr, None)))
                                        if val is None
                                        else repr(self._bytes_to_unicode(val))
                                    ),
                                ))
                            if new_mail or new_grp:
                                print('\tSeafile->[ChangementMail={},NouveauxGroupes={}]'.format(
                                    new_mail,
                                    new_grp,
                                ))
                        
                        if not dry_run:
                            # vérifier si changement de dn :
                            if dn_actuel != nouveau_dn:
                                try:
                                    self.ldap_connexion.rename_s(
                                        dn_actuel,
                                        'cn={}'.format(person['cn']),
                                        newsuperior='ou={},{}'.format(person['ou'], conf.LDAP_BDN),
                                        delold=1,
                                    )
                                    dn_actuel = nouveau_dn
                                except ldap.NO_SUCH_OBJECT:
                                    # besoin de créer l'ou=
                                    if not quiet: print('Création de ou={},{}'.format(person['ou'], conf.LDAP_BDN))
                                    if not dry_run:
                                        self.ldap_connexion.add_s(
                                            "ou={},{}".format(person['ou'], conf.LDAP_BDN),
                                            [
                                                ('objectClass', self._unicode_to_bytes(
                                                    ['organizationalUnit', 'top']
                                                )),
                                                ('ou', self._unicode_to_bytes([person['ou']])),
                                            ]
                                        )
                                    # réessayer le rename
                                    self.ldap_connexion.rename_s(
                                        dn_actuel, 
                                        'cn={}'.format(person['cn']), 
                                        newsuperior='ou={},{}'.format(person['ou'], conf.LDAP_BDN), 
                                        delold=1, 
                                    ) 
                                    dn_actuel = nouveau_dn
                                except ldap.ALREADY_EXISTS:
                                    # problème de doublon
                                    print('Erreur lors du renomage de {} en {}, le deuxième existe déjà.'.format(dn_actuel, nouveau_dn), file=sys.stderr)
                                    print('Lancer `sudo galilee_gestion_ldap recherche_doublon` peut résoudre ce problème.', file=sys.stderr)
                                    continue
                
                            # appliquer les changements dans le LDAP
                            self.ldap_connexion.modify_s(dn_actuel, modlist)
                
                        # si changement de mail -> mise à jour Seafile
                        if new_mail:
                            for ancien_mail in ldap_data['mail']:
                                ancien_mail = self._bytes_to_unicode(ancien_mail)
                                if not self._est_un_mail_galilee(ancien_mail):
                                    if isinstance(person['email'], list):
                                        new_mail_addr = person['email'][0].lower()
                                    else:
                                        new_mail_addr = person['email'].lower()
                                    if not new_mail_addr:
                                        # pas de mail dans jeito, on prends le mail Galilée
                                        for act_mail in ldap_data['mail']:
                                            act_mail = self._bytes_to_unicode(act_mail)
                                            if self._est_un_mail_galilee(act_mail):
                                                new_mail_addr = act_mail
                                    if new_mail_addr:
                                        seaf_mig(
                                            ancien_mail,
                                            new_mail_addr,
                                            self._bytes_to_unicode(ldap_data['uid'][0]),
                                            dry_run=dry_run
                                        )
                                    else:
                                        raise Exception("Erreur : plus aucun mail à associer à ce compte Seafile…")
                    
                        # si changement de o ou de ou -> mise à jour des groupes seafile
                        if new_grp:
                            seaf_set_grp(person['cn'], verbose=(not quiet), dry_run=dry_run)
                        
                        if not quiet: print()
                        
                # sinon créer une nouvelle fiche
                else:
                    dn = "cn={},ou={},{}".format(person['cn'], person['ou'], conf.LDAP_BDN)
                    if not quiet: print('{} {} -> nouvelle fiche {}'.format(person['cn'], person['displayName'], dn))
                    try:
                        if not dry_run:
                            self.ldap_connexion.add_s(dn, _add_request(person) )
                    except ldap.NO_SUCH_OBJECT:
                        # l'ou= n'existe pas, la créer
                        if not quiet: print('Création de ou={},{}'.format(person['ou'], conf.LDAP_BDN))
                        if not dry_run:
                            self.ldap_connexion.add_s(
                                "ou={},{}".format(person['ou'], conf.LDAP_BDN),
                                [
                                    ('objectClass', self._unicode_to_bytes(
                                            ['organizationalUnit', 'top']
                                        )),
                                    ('ou', self._unicode_to_bytes([person['ou']])),
                                ]
                            )
                            # ré-essayer l'ajout de la fiche
                            self.ldap_connexion.add_s(dn, _add_request(person) )
                    except ldap.INVALID_SYNTAX:
                        print('ADD: ldap.INVALID_SYNTAX', file=sys.stderr)
                        print(_add_request(person), file=sys.stderr)
                        sys.exit(43)
                    except ldap.TYPE_OR_VALUE_EXISTS:
                        print("Erreur d'ajout au LDAP `TYPE_OR_VALUE_EXISTS`", file=sys.stderr)
                        print(dn, _add_request(person), file=sys.stderr)
                        sys.exit(42)
                    if not quiet: print() # saut de ligne entre deux fiches
                            
                #### s'il y a changement du statut suspension ou radiation, lever une alerte
                etat_alerte = self.gest_alertes.tester_alertes(person)
                message = self.gest_alertes.message_alerte(person, etat_alerte)
                if message:
                    print(message, file=sys.stderr)

        # enregistrer l'état des alertes
        self.gest_alertes.save()

    def add_display_names(self):
        """Ajoute des attributs displayName à toutes les fiches qui n'ent ont pas"""
        sans_disp_name = self.recherche_numadh('(!(displayName=*))')
        
        for numadh in sans_disp_name:
            data = self.adh_data(numadh, ['givenName','sn'])
            if data:
                try:
                    dispN = '{} {}'.format(
                        data.get('givenName', [''])[0].title(), 
                        data.get('sn', [''])[0].title(), 
                    )
                except KeyError:
                    print('Erreur cn={}'.format(numadh))
                    print(data)
                    return
                print('cn={} +displayName={}'.format(numadh, dispN))
                self.editer_adh(numadh, [('displayName', dispN)])

    ### Utilities

    def _bytes_to_unicode(self, obj):
        """Retourne une représentation unicode des bytes de obj"""
        if isinstance(obj, list):
            return [ self._bytes_to_unicode(i) for i in obj ]
        elif isinstance(obj, tuple):
            return tuple(( self._bytes_to_unicode(i) for i in obj ))
        elif isinstance(obj, dict):
            newdict = {}
            for key in obj:
                newdict[key] = self._bytes_to_unicode(obj[key])
            return newdict
        elif isinstance(obj, bytes):
            return obj.decode('utf-8')
        else:
            return obj

    def _unicode_to_bytes(self, obj):
        """Retourne une représentation bytes des textes de obj"""
        if isinstance(obj, list):
            return [ self._unicode_to_bytes(i) for i in obj ]
        elif isinstance(obj, tuple):
            return tuple(( self._unicode_to_bytes(i) for i in obj ))
        elif isinstance(obj, dict):
            newdict = {}
            for key in obj:
                newdict[key] = self._unicode_to_bytes(obj[key])
            return newdict
        elif isinstance(obj, str):
            return obj.encode('utf-8')
        else:
            return obj
    
    def make_secret(self, password):                                                       
        """                                                                          
        Encodes the given password as a base64 SSHA hash+salt buffer                 
        """
        return ldap_salted_sha1.hash(password)
        

    def backup(self):
        """
        Fait une sauvegarde des données du LDAP
        (à lancer avant une opération critique)
        Ne fonctionne qu'en root !
        
        Renvoie True si le backup a eu lieu, False sinon
        """
        if os.getuid() != 0:
            # pas root
            return False 
        
        bakdir = os.path.join(conf.LDAP_BACKUP, 'galilee-ldap')
        if not os.path.isdir(bakdir):
            os.mkdir(bakdir)
            
        bakfilename = os.path.join(bakdir, 
            '{}-{}.ldif'.format(conf.LDAP_BDN, datetime.datetime.now().isoformat())
        )
        
        subprocess.check_output(
            ['slapcat', '-b', conf.LDAP_BDN, '-l', bakfilename]
        )
        return True

    def _est_un_mail_galilee(self, mail):
        """Renvoie si une adresse mail est un mail de Galilée"""
        try:
            u, d = mail.split('@')
        except ValueError:
            return False
        return (d in conf.TOUS_LES_DOMAINES_MAILS)

    def _makeUidNumber(self, numadh):
        """Renvoie un uidNumber (ou gidNumber) basé sur le numéro d'adhérent·e"""
        try:
            uidNumber = int(numadh)
        except ValueError:
            if numadh.startswith('SAL-'):
                # Pour les salarié·es/SC, on utilise le numéro de salarié·e/SC
                # préfixé par 9 pour éviter les collisions avec les numéros
                # d'adhérent·es
                uidNumber = 9000000 + int(numadh[4:])
            else:
                raise ValueError
        return uidNumber
