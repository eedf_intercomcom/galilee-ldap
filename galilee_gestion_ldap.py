#!/usr/bin/python3
# -*- coding: utf-8 -*-
###############################################################################
#       galilee_gestion_ldap.py
#       
#       Copyright © 2023-2024, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Programme de gestion du LDAP de Galilée"""

import os
import sys
# à améliorer :) spécifique à l'install actuelle sur galilée
sys.path.insert(0, '/usr/local/lib/galilee-ldap/')
import argparse
import argcomplete
import json
import datetime
try:
    import galilee_ldap
except ModuleNotFoundError:
    sys.stderr.write('Impossible de trouver la configuration de Galilée-LDAP\n')
    sys.stderr.write('Est-ce que le programme a été lancé en root ou avec sudo ?\n')
    sys.exit(10)
import galilee_jeito
import galilee_seafile_ldap
from galilee_seafile_ldap import SeafileGroupes, SeafileMigration, SeafileSSO
from ldap import NO_SUCH_OBJECT, SCOPE_SUBTREE
from annuaire import annuaire
from prettytable import PrettyTable                                              

try:
    SEAF_FORCE_DRY_RUN = galilee_seafile_ldap.conf.FORCE_DRY_RUN
except AttributeError:
    SEAF_FORCE_DRY_RUN = False


##### commandes de fusion / gestion des doublons du LDAP #####

def fusion(args):
    """Lancer une fusion de plusieurs fiches adhérent·es"""
    gldap = galilee_ldap.GalileeLDAP()
    
    # faire un backup avant toute chose
    print("Sauvegarde du LDAP dans {}…".format(galilee_ldap.conf.LDAP_BACKUP))
    if not gldap.backup():
        # backup impossible
        sys.stderr.write('Impossible de faire une sauvegarde (pas root ?)\n')
        res = input('Continuer quand même ? [o/N] ')
        if not res.lower() == 'o':
            sys.exit(2)
    else:
        print("Sauvegarde effectuée.")
    print()
    
    try:
        res = gldap.fusion_dn(args.cn_a_garder, args.cn_a_fusionner, (not args.suppr_auto))
        numadh = res[0]
        uid = res[1]
        new_mail = res[2]
        old_mail = res[3:]
    except galilee_ldap.DoublonNonFusionnable as dnf:
        sys.stderr.write("Fusion impossible : {}\n".format(dnf.raison))
        sys.stderr.write("    {}\n".format(dnf.doublon))
        sys.exit(1)
    except NO_SUCH_OBJECT as err:
        sys.stderr.write("Erreur : fusion impossible\n")
        sys.stderr.write("Un des dn donnés n'existe pas.\n")
        sys.stderr.write("(son OU: {})\n".format(err.args[0]['matched']))
        sys.exit(3)
    else:
        print("{}, {}: fusion effectuée.".format(numadh, uid))
        if old_mail:
            print("Ce compte Galilée a changé d'adresse mail, changement à effectuer dans Seafile")
            smig = SeafileMigration()
            for m in old_mail:
                smig.migration_mail(m, new_mail, uid)
        if numadh and uid:
            sg = SeafileGroupes()
            print("Cette fiche a un compte Galilée, vérification des groupes Seafile")
            sg.ajouter_groupes_par_defaut(numadh, dry_run=SEAF_FORCE_DRY_RUN, verbose=True)

def recherche_doublon(args):
    """Lance une recherche de doublons, puis demande interractivement de
    les fusionner.
    """
    gldap = galilee_ldap.GalileeLDAP()
    
    # faire un backup avant toute chose (sauf si --liste)
    if not args.liste:
        print("Sauvegarde du LDAP dans {}…".format(galilee_ldap.conf.LDAP_BACKUP))
        if not gldap.backup():
            # backup impossible
            sys.stderr.write('Impossible de faire une sauvegarde (pas root ?)\n')
            res = input('Continuer quand même ? [o/N] ')
            if not res.lower() == 'o':
                sys.exit(2)
        print("Sauvegarde effectuée.")
        print()
    
    # rechercher les doublons
    if not args.guests:
        if not args.liste:
            print("Recherche des doublons [même numéro d'adhérent·e]… (ça peut prendre du temps)")
        dbl_cn = gldap.recherche_doublon_cn(args.compte)
    else:
        dbl_cn = []
    if not args.liste:
        print("Recherche des doublons [même identité]… (ça peut prendre du temps)")
    dbl_id = gldap.recherche_doublon_id(args.compte, args.guests, args.fuzzy, args.juste_mail)
    
    # afficher les statistiques
    if not args.liste:
        print("{} doublons trouvés :".format(len(dbl_cn) + len(dbl_id)))
        cn_nb_sur = len([d for d in dbl_cn if d.certitude])
        print("    {} doublons de même numéro d'adhérent·e ({} sûr, {} à vérifier)".format(
            len(dbl_cn), cn_nb_sur, len(dbl_cn) - cn_nb_sur
        ))
        id_nb_sur = len([d for d in dbl_id if d.certitude])
        print("    {} doublons de même identité ({} sûr, {} à vérifier)".format(
            len(dbl_id), id_nb_sur, len(dbl_id) - id_nb_sur
        ))
    
    # affichage de type liste
    if args.liste:
        # format :
        # [F/~/?] dn_a_garder autres_dn
        for dbl in (dbl for dbl in dbl_cn + dbl_id if dbl.certitude==True):
            print('F\t{}\t{}'.format(dbl.dn_a_garder, '\t'.join(dbl.dn_a_fusionner)))
        for dbl in (dbl for dbl in dbl_cn + dbl_id if dbl.certitude=='fuzzy'):
            print('~\t{}\t{}'.format(dbl.dn_a_garder, '\t'.join(dbl.dn_a_fusionner)))
        for dbl in (dbl for dbl in dbl_cn + dbl_id if not dbl.certitude==False):
            print('?\t{}'.format('\t'.join(dbl.dn_a_fusionner)))
        sys.exit(0)
         
    
    # sinon lancement des fusions
    print()
    
    sg = SeafileGroupes()
    
    # si fusion automatique
    if args.fusion_auto:
        # lancer les fusions
        for dbl in (dbl for dbl in dbl_cn + dbl_id if dbl.certitude == True):
            try:
                res = gldap.fusion(dbl, demande_avant_suppression=False)
                numadh = res[0]
                uid = res[1]
                new_mail = res[2]
                old_mail = res[3:]
            except (galilee_ldap.DoublonNonFusionnable, NO_SUCH_OBJECT):
                sys.stderr.write("{}: fusion impossible".format(dbl.dn_a_garder))
            else: 
                print("{}, {}: fusion effectuée.".format(numadh, uid))
                if old_mail:
                    print("Ce compte Galilée a changé d'adresse mail, changement à effectuer dans Seafile") 
                    smig = SeafileMigration() 
                    for m in old_mail: 
                        smig.migration_mail(m, new_mail, uid)
                if numadh and uid: 
                    sg.ajouter_groupes_par_defaut(numadh, dry_run=SEAF_FORCE_DRY_RUN, verbose=False)
        
        nb_fuzzy = len([dbl for dbl in dbl_cn + dbl_id if not dbl.certitude == 'fuzzy'])
        nb_sans_fusion = len([dbl for dbl in dbl_cn + dbl_id if not dbl.certitude == False])
        if nb_sans_fusion > 0:
            sys.stderr.write("Attention : il reste {} doublons sans stratégie de fusion.\n".format(nb_sans_fusion))
            sys.stderr.write("et {} doublons avec une stratégie de fusion, mais issus d'une recherche approximative.\n".format(nb_fuzzy))
            sys.stderr.write("Lancer une recherche_doublon sans --fusion-auto pour les voir.")
    
    else:
    # sinon
        print("À tout moment, faire CTRL+C pour quitter.")
        # pour chaque doublon (avec stratégie) :
        for dbl in (dbl for dbl in dbl_cn + dbl_id if dbl.certitude):
            # afficher les infos            
            print(dbl.infos(gldap))
            # demander si fusion à faire
            if dbl.certitude == 'fuzzy':
                print("#### Attention : doublon issus d'une recherche approximative (fuzzy) #### ", end='')
            res = input("Effectuer la fusion ? [o/N] ")
            if res.lower() == 'o':
                try:
                    res = gldap.fusion(dbl, demande_avant_suppression=False)
                    numadh = res[0]
                    uid = res[1] 
                    new_mail = res[2]
                    old_mail = res[3:]
                except galilee_ldap.DoublonNonFusionnable as dnf: 
                    sys.stderr.write("Fusion impossible : {}\n".format(dnf.raison))
                    sys.stderr.write("    {}\n".format(dnf.doublon))
                except NO_SUCH_OBJECT as err:
                    sys.stderr.write("Erreur : fusion impossible\n")
                    sys.stderr.write("Un des dn donnés n'existe pas.\n")
                else:  
                    print("{}, {}: fusion effectuée.".format(numadh, uid))
                    if old_mail:
                        print("Ce compte Galilée a changé d'adresse mail, changement à effectuer dans Seafile") 
                        smig = SeafileMigration() 
                        for m in old_mail: 
                            smig.migration_mail(m, new_mail, uid)
                    if numadh and uid:  
                        sg.ajouter_groupes_par_defaut(numadh, dry_run=SEAF_FORCE_DRY_RUN, verbose=True) 
            else:
                print("Pas de fusion, aucune modification effectuée")
            print()
            
        res = input("Afficher les doublons sans stratégie de fusion ? [O/n] ")
        if res.lower() != 'n':
            for dbl in (dbl for dbl in dbl_cn + dbl_id if not dbl.certitude):
                # afficher les infos
                print(dbl.infos(gldap))
                print("Pas de stratégie de fusion, tu peux faire une fusion manuelle avec :")
                print("galilee_gestion_ldap fusion dn_à_garder dn_a_fusionner")
                input("Appuie sur entrée pour afficher le doublon suivant.")
                print()

##### import / synchro avec jeito #####

def import_jeito(args):
    """Synchroniser jeito dans le LDAP"""
    if args.fonctions:
        ja = galilee_jeito.JeitoAPI()
        (fonc_set, stable_fonc_set) = ja.fonctions()
        print("*** fonctions stables ***")
        for fonc in stable_fonc_set:
            print(fonc)
        print()
        print("*** fonctions définies dans Jeito (les noms peuvent changer) ***")
        for fonc in fonc_set:
            print(fonc)
    else:
        gldap = galilee_ldap.GalileeLDAP()
        sg = SeafileGroupes()
        smig = SeafileMigration()  
    
        gldap.synchro(
            seaf_mig=smig.migration_mail,
            seaf_set_grp=sg.ajouter_groupes_par_defaut,
            quiet=args.quiet,
            dry_run=args.dry_run,
        )


def menage_ldap(args):
    """Lister puis proposer de supprimer les entrées obsolètes du LDAP""" 
    gldap = galilee_ldap.GalileeLDAP() 

    a_supprimer = gldap.liste_dn_a_supprimer(args.annee)
    
    print('À supprimer :')
    for dn, saison in a_supprimer:
        print('{}, {}'.format(dn, saison))
    print('{} entrées à supprimer.'.format(len(a_supprimer)))
    
    if len(a_supprimer) == 0:
        sys.exit(0)
    
    print()
    
    res = input('Lancer la suppression de ces entrées ? [o/N] ')
    if not res.lower() == 'o':
        print("Annulation, rien n'a été supprimé.")
        sys.exit(0)

    # faire un backup avant la suppression 
    print("Sauvegarde du LDAP dans {}…".format(galilee_ldap.conf.LDAP_BACKUP)) 
    if not gldap.backup(): 
        # backup impossible 
        sys.stderr.write('Impossible de faire une sauvegarde (pas root ?)\n') 
        res = input('Continuer quand même ? [o/N] ') 
        if not res.lower() == 'o': 
            sys.exit(2) 
    else: 
        print("Sauvegarde effectuée.") 
    print() 

    print('Suppression…')
    nb = gldap.menage(adh[0] for adh in a_supprimer)
    print('{} entrées ont été supprimées.'.format(nb))

def mkannuaire(args):
    """Produit un annuaire html sur la sortie standard"""
    print(annuaire(args.region, args.template))

def structures(args):
    """Afficher les structures connues dans le LDAP"""
    region = args.region
    gldap = galilee_ldap.GalileeLDAP()
    if region:
        print("Attention : cette commande liste les structures dont sont membres")
        print("les adhérents de cette région/ce périmètre. Comme on peut être membre")
        print("de plusieurs structures, certaines structures peuvent être listées dans")
        print("plusieurs régions/périmètres.")
        print()
        res = gldap.ldap_connexion.search_s(                                     
            gldap.filter_structure + gldap.bdn,                                
            SCOPE_SUBTREE,                                                   
            '(ou={})'.format(region),                      
            ['o']
        )
        res = gldap._bytes_to_unicode(res)
        # rendre la liste unique avec un set
        o_set = set()
        for dn, attr in res:
            try:
                for o_item in attr['o']:
                    o_set.add(o_item)
            except KeyError:
                # pas d'attr o
                pass
        
        # lister les données des deux colones
        names = ["Nom dans le LDAP"]
        filters = ["Filtre LDAP"]
        for o_itm in o_set:
            name = o_itm
            names.append('"{}"'.format(name)) 
            filters.append('(o={})'.format(name))
    else:
        res = gldap.ldap_connexion.search_s(                                     
            gldap.filter_structure + gldap.bdn,                                
            SCOPE_SUBTREE,                                                   
            '(objectClass=organizationalUnit)',                      
            ['ou']
        )
        res = gldap._bytes_to_unicode(res)
        
        # lister les données des deux colones
        names = ["Nom dans le LDAP"]
        filters = ["Filtre LDAP"]
        for dn, attr in res:
            name = attr['ou'][0]
            names.append('"{}"'.format(name))
            filters.append('(ou={})'.format(name))
    
    # calcul affichage
    taillemax = max(len(i) for i in names)
    taillecol1 = taillemax + 4
        
    # affichage
    for i in range(len(names)):
        name = names[i]
        filt = filters[i]
        print('{}{}{}'.format(
            name,
            ' ' * (taillecol1 - len(name)),
            filt,
        ))

def alertes(args):
    """Gestions des alertes"""
    if args.desactiver_cpt:
        gldap = galilee_ldap.GalileeLDAP()
        print("Désactiver le compte de {}…".format(args.desactiver_cpt))
        gldap.desactiver_cpt(args.desactiver_cpt)
        print("Fait.")
        print("Attention :")
        print("Cela n'a pas suspendu ses abonnements à des listes e-mail.")
        print("Si besoin, il faut faire cela manuellement sur https://listes.eedf.fr")
    elif args.reactiver_cpt:
        gldap = galilee_ldap.GalileeLDAP()
        gldap.reactiver_cpt(args.reactiver_cpt)
        print("Le compte de {} a été ré-activé.".format(args.reactiver_cpt))
        print("Il faut maintenant lui dire d'aller ré-initialiser son mot de passe sur :")
        print("https://galilee.eedf.fr/utilisateurices/changer_mdp/")
        print("Si des abonnements e-mail de cette personne avait été désactivé,")
        print("il faut aller les ré-activer manuellement sur https://listes.eedff.r/")
    elif args.fait:
        cn = args.fait
        if not cn.startswith('cn='):
            cn = 'cn=' + cn
        gest_alertes = galilee_jeito.GestionAlertes()
        # demander les infos de la situation
        print(gest_alertes.commande_gestion_alerte(args.fait))
        print()
        rep = input("As-tu bien suivi la procédure ci-dessus ? [o/N]")
        if rep.lower() != 'o':
            print("Action annulée.")
            return
        print("Qui a géré cette situation à l'InterComCom ?")
        par_qui = input()
        print("Le contact de la personne aux éclés en référence à cette situation ?")
        contact = input()
        print("En une ligne, ce qui a été fait pour gérer cette alerte.")
        commentaire = input()
        # marquer une alerte comme géré
        jeito_api = galilee_jeito.JeitoAPI()
        msg = gest_alertes.alerte_geree(jeito_api, cn, par_qui, contact, commentaire)
        print(msg)
    else:
        # afficher une synthèse
        jeito_api = galilee_jeito.JeitoAPI()
        gest_alertes = galilee_jeito.GestionAlertes()
        print(gest_alertes.synthese_alerte(jeito_api))
        print(gest_alertes.commande_gestion_alerte())


##### gestion des comptes seafile #####

def ajout_groupes_seafile(args):
    """Ajouter automatiquement les utilisateurices à des groupes seafile
    correspondant à leurs structures."""
    sg = SeafileGroupes()
    if args.num_adh:
        sg.ajouter_groupes_par_defaut(args.num_adh, (args.dry_run or SEAF_FORCE_DRY_RUN), verbose=True)
    else:
        sg.ajouter_tous_groupes_defaut(args.structure, (args.dry_run or SEAF_FORCE_DRY_RUN), verbose=True) 


def migration(args):
    """Migre un compte seafile d'un ancien mail vers un nouveau"""
    # récupérer nom utilisateurice du compte
    gldap = galilee_ldap.GalileeLDAP()
    res = gldap.recherche_numadh('(&(uid=*)(|(mail={})(mail={})))'.format(
        args.ancien_mail, args.nouveau_mail
    ))
    uid = gldap.adh_data(res[0], ['uid'])['uid'][0]
    # effectuer la migration
    seamig = SeafileMigration()
    seamig.migration_mail(args.ancien_mail, args.nouveau_mail, uid)

def seaf_auth(args):
    """Affichage ou changement de SSO dans seafile"""
    mail_id = args.mail_id
    if not args.switch_sso:
        seafsso = SeafileSSO()
        info = seafsso.auth_info(mail_id)
        if not info:
            print("{} n'a pas été trouvé.")
            sys.exit(3)
        
        table = PrettyTable()
        table.field_names = ["identifiant", "hash password", "SSO", "ID SSO", "extra_data"]
        table.add_row([
            info['mail_id'],
            info['password_hash'],
            info['provider'],
            info['provider_uid'],
            info['extra_data'],
        ])
        print(table.get_string())
        #if info['provider']:
        #    print("Test de récupération de l'ID seafile depuis l'ID SSO")
        #    if info['provider'] == 'ldap':
        #        print(seafsso.get_seaf_id_ldap(info['provider_uid']))
        #    else: #oidc
        #        print(seafsso.get_seaf_id_oidc(info['provider_uid']))
        
        seafsso.close()
    else:
        if args.switch_sso != 'none' and not args.sso_id:
            print("SSO_ID est requis pour un changement de SSO", file=sys.stderr)
            sys.exit(2)
        if args.switch_sso == 'none':
            print("Supprimer l'authentification SSO pour ", mail_id) #TODO
            seafsso = SeafileSSO()
            seafsso.change_auth_native(mail_id)
            seafsso.close()
        else:
            print("Changer de SSO pour {} : sso={}, id={}".format(
                mail_id, args.switch_sso, args.sso_id))
            if args.switch_sso == galilee_seafile_ldap.conf.LDAP_SSO_ID:
                seafsso = SeafileSSO()
                seafsso.change_auth_ldap(mail_id, args.sso_id)
                seafsso.close()
            elif args.switch_sso == galilee_seafile_ldap.conf.OIDC_SSO_ID:
                seafsso = SeafileSSO()
                seafsso.change_auth_oidc(mail_id, args.sso_id)
                seafsso.close()
            else:
                print("{} n'est pas un identifiant SSO reconnu.".format(args.sso_id), file=sys.stderr)
                sys.exit(2)

def seaf_init_cpt(args):
    """Affichage ou changement de SSO dans seafile"""
    num_adh = args.num_adh
    galilee_seafile_ldap.init_seafile_account(num_adh, verbose=True)

def seaf_check_sso(args):
    """Gestion des SSO de Seafile"""
    if args.fix_ldap:
        seafsso = SeafileSSO()
        seafsso.fix_ldap()
    elif args.switch_to_oidc:
        seaf_mig = SeafileMigration()
        seaf_mig.migration_ldap_vers_oidc()
        print()
        print("Migration effectuée !")
        print("Reste à faire :")
        print("     - changer le paramètre OICD_REMPLACE_LDAP dans la config")
        print("     - changer la page de login de Seafile")
        print("     - changer la doc de Seafile")
    elif args.switch_resend_gmail:
        seaf_mig = SeafileMigration()                                            
        seaf_mig.migration_ldap_vers_oidc(mail_pour_gmail=True)
    else:
        print("Manque une action")

def fix(args):
    """Corrections dans le LDAP"""
    if args.ajout_display_names:
        gldap = galilee_ldap.GalileeLDAP()
        gldap.add_display_names()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='commandes', 
        description='commandes pour modifier le LDAP', required=True)

    ## gestion des doublons / fusions de fiches LDAP

    pfusion = subparsers.add_parser('fusion', help='fusionner deux fiches adhérent·es')
    pfusion.add_argument('--suppr-auto', action='store_true',
        help="Ne pas demander avant de supprimer une fiche adhérent·e")
    pfusion.add_argument('cn_a_garder')
    pfusion.add_argument('cn_a_fusionner', nargs='+')
    pfusion.set_defaults(func=fusion)
    
    p_rch_dbl = subparsers.add_parser('recherche_doublon', 
        help='lancer une recherche de doublons pour les fusionner')
    p_rch_dbl.add_argument('-c', '--compte', 
        help="rechercher uniquement parmis les fiches avec un compte Galilée associé",
        action="store_true")
    p_rch_dbl.add_argument('-g', '--guests',
        help="rechercher uniquement dans ou=guests (comptes hors fichiers adhérent)",
        action="store_true")
    p_rch_dbl.add_argument('-f', '--fuzzy', action='store_true',
        help="Fait une recherche approximative sur les noms et prénoms au lieu d'une recherche exacte (à utiliser principalement avec -g)")
    p_rch_dbl.add_argument('-m', '--juste-mail', action='store_true',
        help="Ne se base que sur le mail pour les recherches de similitude d'identité (à utiliser principalement avec -g). Ces doublons n'auront jamais de fusion proposée")
    p_rch_dbl.add_argument('-l', '--liste', action='store_true',
        help="Ne fait que lister les doublons, sous une forme scriptable")
    p_rch_dbl.add_argument('-a', '--fusion-auto',
        help="fusionner automatiquement les doublons si l'action à faire est sûre",
        action="store_true")
    p_rch_dbl.set_defaults(func=recherche_doublon)

    ## gestion de l'import jeito

    p_imp_je = subparsers.add_parser('import_jeito',
        help='télécharger la base adhérent·e sur jeito, et la synchroniser dans le LDAP')
    p_imp_je.add_argument('-f', '--fonctions',
        help="afficher la liste des fonctions utilisées dans Jeito",
        action="store_true")
    p_imp_je.add_argument('-q', '--quiet', action='store_true',
        help="n'affiche pas les opérations à faire, seules les infos importantes seront affichées sur stderr")
    p_imp_je.add_argument('--dry-run', action='store_true',
        help="affiche les opérations à faire, sans les appliquer réellement")
    p_imp_je.set_defaults(func=import_jeito)

    p_menage_ldap = subparsers.add_parser('menage_ldap',
        help='Enlever les entrées du LDAP ayant plus d\'un an sans adhésion')
    def_annee_menage = datetime.date.today().year-1
    p_menage_ldap.add_argument('--annee', type=int, 
        default=(def_annee_menage),
        help="Par ex, si 2022, supprime les entrées jusqu'à la saison 2021-2022. "
             "Si non précisé, annee={}, ce qui doit être bon pour une "
             "suppression en août.".format(def_annee_menage)
        )
    p_menage_ldap.set_defaults(func=menage_ldap)

    p_annuaire = subparsers.add_parser('annuaire',
        help="Produire un annuaire html pour une région")
    p_annuaire.add_argument('region')
    p_annuaire.add_argument('template')
    p_annuaire.set_defaults(func=mkannuaire)
    
    p_struct = subparsers.add_parser('structures',
        help="Afficher la liste des structures connues dans le LDAP (que les régions par défaut)")
    p_struct.add_argument('--region', type=str,
        help="Afficher les structures de cette région/ce périmètre")
    p_struct.set_defaults(func=structures)

    p_alerte = subparsers.add_parser('alertes',
        help="Gérer les alertes de suspensions/exclusions d'adhérent·es. Par défaut, affiche une synthèse des alertes.")
    p_alerte.add_argument('--fait', type=str, metavar='NUM_ADH',
        help="Marqué l'alerte concernant NUM_ADH comme gérée.")
    p_alerte.add_argument('--desactiver-cpt', type=str, metavar="NUM_ADH",
        help="Désactiver l'accès au compte Galilée de NUM_ADH")
    p_alerte.add_argument('--reactiver-cpt', type=str, metavar="NUM_ADH",
        help="Ré-activer l'accès au compte Galilée de NUM_ADH")
    p_alerte.set_defaults(func=alertes)

    # corrections dans le LDAP
    p_fix = subparsers.add_parser('corrections_ldap',
        help="Effectue des corrections dans le LDAP")
    p_fix.add_argument('--ajout-display-names', action='store_true',
        help="Ajout les attributs DisplayName qui manquent")
    p_fix.set_defaults(func=fix)

    # gestion des comptes seafile

    p_grp_seaf = subparsers.add_parser('seafile_ajout_groupes',
        help='ajouter à toustes les utilisateurices les groupes seafile de leurs structures')
    p_grp_seaf.add_argument('--num-adh', type=str,
        help="N'ajouter les groupes que pour NUM_ADH")
    p_grp_seaf.add_argument('--structure',
        help='identifiant de la structure, pour limiter l\'ajout à celle-ci')
    p_grp_seaf.add_argument('--dry-run', action='store_true',
        help='afficher les opérations à faire sans les appliquer réellement')
    p_grp_seaf.set_defaults(func=ajout_groupes_seafile)

    p_migration = subparsers.add_parser('seafile_migration',
        help="Change le mail d'un compte Seafile")
    p_migration.add_argument('ancien_mail')
    p_migration.add_argument('nouveau_mail')
    p_migration.set_defaults(func=migration)

    # uniquement pour tests
    #p_init_cpt = subparsers.add_parser('seafile_init_cpt',
    #    help="Initialiser le compte Seafile d'un fiche adhérent·e")
    #p_init_cpt.add_argument('num_adh', type=str,
    #    help="Numéro d'adhérent·e ou identifiant SAL-*")
    #p_init_cpt.set_defaults(func=seaf_init_cpt)

    if galilee_seafile_ldap.conf.SEAFVERSION >= 11:
        p_auth = subparsers.add_parser('seafile_auth',
            help="Affiche ou change le système d'authentification d'un·e utilisateurice")
        p_auth.add_argument('mail_id', type=str,
            help="identifiant (type mail) Seafile du compte")
        p_auth.add_argument('--switch-sso', type=str,
            help="identifiant du SSO vers lequel switcher, ou 'none' pour une authentification interne")
        p_auth.add_argument('--sso-id', type=str,
            help="identifiant du compte dans le SSO de destination")
        p_auth.set_defaults(func=seaf_auth)
        
        if not galilee_seafile_ldap.conf.OICD_REMPLACE_LDAP:
            p_seaf_sso = subparsers.add_parser('seafile_sso',
                help="Gestion globale du SSO de Seafile")
            p_seaf_sso.add_argument('--fix-ldap', action="store_true",
                help="Met à jour les identifiants LDAP suite à la mise à jour de Seafile 11")
            p_seaf_sso.add_argument('--switch-to-oidc', action="store_true",
                help="Migre toustes les utilisateurices de Seafile vers OpenIDConnect")
            p_seaf_sso.add_argument('--switch-resend-gmail', action="store_true",
                help="Renvoie les mails d'info aux utilisateurices gmail")
            p_seaf_sso.set_defaults(func=seaf_check_sso)
            
        
    argcomplete.autocomplete(parser)
    
    args = parser.parse_args()
    try:
        args.func(args)
    except KeyboardInterrupt:
        # ctrl-c
        print()
        sys.exit(0)
    
    
    
