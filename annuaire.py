# -*- coding: utf-8 -*-
###############################################################################
#       tools/annuaire.py
#       
#       Copyright © 2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
""" synchronisation d'un annuaire html d'une région avec le LDAP """

import sys
import os
from jinja2 import Environment, FileSystemLoader
import galilee_ldap
import datetime

COL_LIST = (
('cn', 'Numéro adhérent·e'),                    #Charfield
('sn', 'Nom'),                                  #Charfield
('givenName', 'Prénom'),                        #Charfield
('schacDateOfBirth', 'Date de Naissance'),      #Datefield
('schacPlaceOfBirth', 'Lieu de Naissance'),     #Charfield
('postalAddress', 'Adresse'),                   #Charfield
('postalCode', 'Code Postal'),                  #Charfield
('l', 'Ville'),                                 #Charfield
('mobile', 'Portable'),                         #Charfield
('homePhone', 'Fixe'),                          #Charfield
('mail', 'Mail'),                               #Emailfield
('title', 'Fonction'),                          #Choicefield
('uid', 'Utilisateurice'),                      #Charfield
('st', 'Département'),                          #Choicefield
('o', 'Structure'),                             #Choicefield
('ou', 'Branche'),                              #Choicefield
('membershipSeason', 'Adhésion'),
)

def annuaire(region, template):
    """Retourne le code html d'un annuaire pour `region` avec `template`"""

    gldap = galilee_ldap.GalileeLDAP(region)    

    env = Environment(loader=FileSystemLoader(os.path.dirname(template)))
    tpl_name = os.path.basename(template)

    dataraw = gldap.all()
    data = []
    nb = len(dataraw)
    for entry in dataraw:
        ligne = []
        for champ in COL_LIST:
            if champ[0] in entry[1]:
                valeur_list = entry[1][champ[0]]
                ligne.append(', '.join(valeur_list))
            else:
                ligne.append('')
        data.append(ligne)

    template = env.get_template(tpl_name)
    return(template.render(
        colaff=COL_LIST,
        data=data,
        nb_l=nb,
        today=datetime.date.today().strftime("%d/%m/%Y"),
    ))

